package com.granberg.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(InstrumentType.class)
public class InstrumentType_ { 

    public static volatile SingularAttribute<InstrumentType, Integer> instrumentTypeId;
    public static volatile SingularAttribute<InstrumentType, String> instrumentTypeName;

}