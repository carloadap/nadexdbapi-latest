package com.granberg.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(Instrument.class)
public class Instrument_ { 

    public static volatile SingularAttribute<Instrument, Integer> instrumentTypeId;
    public static volatile SingularAttribute<Instrument, BigDecimal> instrumentPrice;
    public static volatile SingularAttribute<Instrument, String> instrumentName;
    public static volatile SingularAttribute<Instrument, Date> updatedOn;
    public static volatile SingularAttribute<Instrument, Integer> instrumentId;

}