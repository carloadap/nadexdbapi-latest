package com.granberg.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(OrderType.class)
public class OrderType_ { 

    public static volatile SingularAttribute<OrderType, Integer> orderTypeId;
    public static volatile SingularAttribute<OrderType, String> orderTypeName;

}