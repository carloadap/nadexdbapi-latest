package com.granberg.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(News.class)
public class News_ { 

    public static volatile SingularAttribute<News, Integer> newsId;
    public static volatile SingularAttribute<News, String> importance;
    public static volatile SingularAttribute<News, String> previous;
    public static volatile SingularAttribute<News, String> forecast;
    public static volatile SingularAttribute<News, String> events;
    public static volatile SingularAttribute<News, Date> updatedDate;
    public static volatile SingularAttribute<News, String> actual;
    public static volatile SingularAttribute<News, Date> date;
    public static volatile SingularAttribute<News, String> currency;

}