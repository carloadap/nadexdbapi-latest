package com.granberg.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(Ticket.class)
public class Ticket_ { 

    public static volatile SingularAttribute<Ticket, Date> createdOn;
    public static volatile SingularAttribute<Ticket, Integer> positionSize;
    public static volatile SingularAttribute<Ticket, Integer> ticketId;
    public static volatile SingularAttribute<Ticket, String> status;
    public static volatile SingularAttribute<Ticket, Integer> positionId;
    public static volatile SingularAttribute<Ticket, BigDecimal> profitTarget;
    public static volatile SingularAttribute<Ticket, Integer> contractId;
    public static volatile SingularAttribute<Ticket, BigDecimal> maxProfit;
    public static volatile SingularAttribute<Ticket, Date> updatedOn;
    public static volatile SingularAttribute<Ticket, BigDecimal> maxLoss;

}