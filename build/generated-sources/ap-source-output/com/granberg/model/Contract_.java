package com.granberg.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-04T22:20:02")
@StaticMetamodel(Contract.class)
public class Contract_ { 

    public static volatile SingularAttribute<Contract, Integer> periodicityId;
    public static volatile SingularAttribute<Contract, Date> expirationDate;
    public static volatile SingularAttribute<Contract, Long> floor;
    public static volatile SingularAttribute<Contract, Integer> orderTypeId;
    public static volatile SingularAttribute<Contract, Long> ceiling;
    public static volatile SingularAttribute<Contract, String> active;
    public static volatile SingularAttribute<Contract, Integer> contractId;
    public static volatile SingularAttribute<Contract, Integer> optionTypeId;
    public static volatile SingularAttribute<Contract, Date> updatedOn;
    public static volatile SingularAttribute<Contract, Integer> instrumentId;

}