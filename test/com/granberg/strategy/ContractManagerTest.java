/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.Contract;
import com.granberg.model.Instrument;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class ContractManagerTest {

    public ContractManagerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of OnContractOpen method, of class ContractManager.
     */
    @Test
    public void testOnContractOpen() {
    }

    /**
     * Test of OnContractClose method, of class ContractManager.
     */
    @Test
    public void testOnContractClose() {
    }

    /**
     * Test of OnPriceCrossesContract method, of class ContractManager.
     */
    @Test
    public void testOnPriceCrossesContract() {
    }

    /**
     * Test of HowMuchTimeLeftForContactInMinutes method, of class
     * ContractManager.
     */
    @Test
    public void testHowMuchTimeLeftForContactInMinutes() {
        try {
            Date currentTime = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date exiprationDate = dateFormat.parse("2013-07-30 19:00:00");
            long diffInCurrentTime = exiprationDate.getTime() - currentTime.getTime();
            long diffInCurrentTimeInMinutes = diffInCurrentTime / (1000 * 60);
            System.out.println(diffInCurrentTimeInMinutes);
        } catch (ParseException ex) {
            Logger.getLogger(ContractManagerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of getPercentageRemainingTime method, of class ContractManager.
     */
    @Test
    public void testGetPercentageRemainingTime() {
        try {
            Date currentTime = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date exiprationDate = dateFormat.parse("2013-07-30 19:00:00");
            Date openingDate = dateFormat.parse("2013-07-28 22:00:00");
            long diffInCurrentTime = exiprationDate.getTime() - currentTime.getTime();
            long diffInCurrentTimeInMinutes = diffInCurrentTime / (1000 * 60);
            long diff = exiprationDate.getTime() - openingDate.getTime();
            long diffInMinutes = diff / (1000 * 60);
//            BigDecimal percentage = BigDecimal.valueOf(diffInCurrentTimeInMinutes).divide(BigDecimal.valueOf(diffInMinutes));
            DecimalFormat formatter = new DecimalFormat("##.##");
            double percentage = (double) diffInCurrentTimeInMinutes / diffInMinutes;
            double test = (double) 1 - percentage;
            double test2 = (double) test * 100;
            System.out.println(diffInCurrentTimeInMinutes);
            System.out.println(diffInMinutes);
            System.out.println(percentage);
            System.out.println(test);
            System.out.println(formatter.format(test2));
        } catch (ParseException ex) {
            Logger.getLogger(ContractManagerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of getActiveContract method, of class ContractManager.
     */
    @Test
    public void testGetActiveContract() {

        ContractManager contractManager = ContractManager.getInstance();
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        Instrument instrument = dBAccess.findInstrumentByName("EUR/JPY");
        List<Contract> contractList= contractManager.getNearestActiveContract(instrument);
        for (Contract contract : contractList) {
            System.out.println(contract.getContractDescription());
        }
    }

    /**
     * Test of getCurrentContractPrice method, of class ContractManager.
     */
    @Test
    public void testGetCurrentContractPrice() {
    }

    @Test
    public void getCheapestContract() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
//        Contract contract = dBAccess.findContractById(7201);
//        BigDecimal strikePrice = contract.getStrikePrice();
//        Date expirationDate = contract.getExpirationDate();
//        System.out.println(strikePrice);
//        System.out.println(expirationDate);
//        double newStrikePrice = 0.80;
//        BigDecimal newPrice = strikePrice.add(BigDecimal.valueOf(newStrikePrice));
//        System.out.println(newPrice);
//        Contract cheapestContract = dBAccess.findContractByStrikePriceAndExpirationDate(newPrice, expirationDate);
//        System.out.println(cheapestContract.getContractDescription());
    }
}