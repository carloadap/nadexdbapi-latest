/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.Forex;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math3.analysis.function.Max;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class ATRManagerTest {

    public ATRManagerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetATR() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<Forex> list = dBAccess.findNForexBySymbol("EUR/JPY", 20);
        List<Double> TRList = new LinkedList<>();
        double highLow = 0.00;
        double highPreviousClose = 0.00;
        double lowPreviousClose = 0.00;
        double TR = 0.00;
        double previousClose = 0.00;
        boolean firstData = true;
        int end = 14;
        double TRSum = 0.00;
        double previousATR = 0.00;
        for (int i = 0; i < list.size(); i++) {
            Forex test = list.get(i);
            System.out.println("High: " + test.getHigh());
            System.out.println("Low: " + test.getLow());
            System.out.println("Close: " + test.getClose());
            highLow = test.getHigh().doubleValue() - test.getLow().doubleValue();
            System.out.println("High - Low: " + highLow);
            if (!firstData) {
                highPreviousClose = Math.abs(test.getHigh().doubleValue() - previousClose);
                lowPreviousClose = Math.abs(test.getLow().doubleValue() - previousClose);
            }
            System.out.println("High - Previous Close: " + highPreviousClose);
            System.out.println("Low - Previous Close: " + lowPreviousClose);
            previousClose = test.getClose().doubleValue();
            TR = Math.max(Math.max(highLow, highPreviousClose), lowPreviousClose);
            System.out.println("TR: " + TR);
            TRSum += TR;
            firstData = false;
            if (i == end - 1) {
                System.out.println("TR SUM: " + TRSum);
                previousATR = TRSum / end;
            } else if (i > end - 1) {
                previousATR = (previousATR * 13 + TR) / end;
            }
            System.out.println("ATR: " + previousATR);
        }
    }
    
    @Test
    public void testIDB(){
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
    }
}