/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.InstrumentPriceLog;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Carlo
 */
public class BollingerManagerTest {

    public BollingerManagerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class BollingerManager.
     */
    @Test
    public void testGetInstance() {
    }

    /**
     * Test of evaluate method, of class BollingerManager.
     */
    @Test
    public void testEvaluate() {
    }

    /**
     * Test of processForexHistory method, of class BollingerManager.
     */
    @Test
    public void testProcessForexHistory() {
    }

    /**
     * Test of convertEMAListToArray method, of class BollingerManager.
     */
    @Test
    public void testConvertEMAListToArray() {
    }

    /**
     * Test of getEMADeviation method, of class BollingerManager.
     */
    @Test
    public void testGetEMADeviation_doubleArr() {
    }

    /**
     * Test of getSMA method, of class BollingerManager.
     */
    @Test
    public void testGetSMA() {
    }

    /**
     * Test of getEMA method, of class BollingerManager.
     */
    @Test
    public void testGetEMA() {
    }

    /**
     * Test of getHBAND method, of class BollingerManager.
     */
    @Test
    public void testGetHBAND() {
    }

    /**
     * Test of getLBAND method, of class BollingerManager.
     */
    @Test
    public void testGetLBAND() {
    }

    /**
     * Test of getCount method, of class BollingerManager.
     */
    @Test
    public void testGetCount() {
    }

    /**
     * Test of setCount method, of class BollingerManager.
     */
    @Test
    public void testSetCount() {
    }

    /**
     * Test of getSUM method, of class BollingerManager.
     */
    @Test
    public void testGetSUM() {
    }

    /**
     * Test of setSUM method, of class BollingerManager.
     */
    @Test
    public void testSetSUM() {
    }

    /**
     * Test of getEMAList method, of class BollingerManager.
     */
    @Test
    public void testGetEMAList() {
    }

    /**
     * Test of setEMAList method, of class BollingerManager.
     */
    @Test
    public void testSetEMAList() {
    }

    /**
     * Test of getOldEMA method, of class BollingerManager.
     */
    @Test
    public void testGetOldEMA() {
    }

    /**
     * Test of setOldEMA method, of class BollingerManager.
     */
    @Test
    public void testSetOldEMA() {
    }

    /**
     * Test of getEMADeviation method, of class BollingerManager.
     */
    @Test
    public void testGetEMADeviation_0args() {
    }

    /**
     * Test of setEMADeviation method, of class BollingerManager.
     */
    @Test
    public void testSetEMADeviation() {
    }

    /**
     * Test of getEMAArray method, of class BollingerManager.
     */
    @Test
    public void testGetEMAArray() {
    }

    /**
     * Test of setEMAArray method, of class BollingerManager.
     */
    @Test
    public void testSetEMAArray() {
    }

    /**
     * Test of getPeriod method, of class BollingerManager.
     */
    @Test
    public void testGetPeriod() {
    }

    /**
     * Test of setPeriod method, of class BollingerManager.
     */
    @Test
    public void testSetPeriod() {
    }

    /**
     * Test of getDeviation method, of class BollingerManager.
     */
    @Test
    public void testGetDeviation() {
    }
}