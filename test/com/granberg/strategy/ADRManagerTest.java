/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.HourlyPips;
import com.granberg.model.InstrumentPriceLog;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Carlo
 */
public class ADRManagerTest {

    public ADRManagerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPipsLeft method, of class ADRManager.
     */
    @Test
    public void testGetPipsLeft() {
        Date currentDateHour = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDateHour);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentDateHour = calendar.getTime();
        System.out.println(currentDateHour);
        calendar.add(Calendar.HOUR, 4);
        Date targetDateHour = calendar.getTime();
        System.out.println(targetDateHour);
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        String currentTimeStr = sdf.format(currentDateHour);
        String targetTimeStr = sdf.format(targetDateHour);
        long targetMoveHourInMinutes = (targetDateHour.getTime() - currentDateHour.getTime()) / (1000 * 60);
        System.out.println(targetMoveHourInMinutes);
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<HourlyPips> getHourlyPips = dBAccess.getHourlyPips("AUD/USD", 30, 4);
        int currentHourPips = 0;
        int targetHourPips = 0;
        for (HourlyPips hourlyPips : getHourlyPips) {
            Date date = new Date();
            int startHour = Integer.valueOf(hourlyPips.getHourlyPipsPK().getStartHour());
            int endHour = Integer.valueOf(hourlyPips.getHourlyPipsPK().getEndHour());
            int currentHour = Integer.valueOf(currentTimeStr);
            if (currentHour >= startHour && currentHour < endHour) {
                currentHourPips = hourlyPips.getPip();
            }
        }
//        int currentHourPips = 11;
//        int targetHourPips = 14;
//        int pipsMove = targetHourPips - currentHourPips;
        System.out.println(currentHourPips);
        Date currentDateTime = new Date();
        System.out.println(currentDateTime);
        long timeLeft = (targetDateHour.getTime() - currentDateTime.getTime()) / (1000 * 60);
        System.out.println(timeLeft);
        double timeLeftPercentage = (double) timeLeft / targetMoveHourInMinutes;
        System.out.println(timeLeftPercentage);
        double pipsLeft = timeLeftPercentage * currentHourPips;
        System.out.println(pipsLeft);
        //double currentTimePercentage = (double) currentDateTime.getTime() / targetMoveHourInMinutes;
        //currentHour = CurrentTime ( 02 : 30 ) = 02 : 00
        //targetHour = currentHour + 1
        //targetMoveHourInMinutes = targetHour - currentHour
        //currentHourPips = 11
        //tartgetHourPips = 14
        //pipsMove = targetHourPips - currentHourPips = 3
        //currentTime = current time now
        //currenTimePercentage = currentTime / targetMoveHourInMinutes
        //pipsLeft = currentTimePercentage * pipsMove
    }

    @Test
    public void convertStringToDate() {
        String timeStr = "1200";
        String targetTimeStr = "1400";
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
        String dateStr = dateFormat.format(date);
        int time = Integer.valueOf(timeStr);
        int targetTime = Integer.valueOf(targetTimeStr);
        int currentTime = Integer.valueOf(dateStr);
        System.out.println(time + ", " + targetTime);
        System.out.println(dateStr);
    }

    /**
     * Test of getInstance method, of class ADRManager.
     */
    @Test
    public void testGetInstance() {
    }

    /**
     * Test of evaluate method, of class ADRManager.
     */
    @Test
    public void testEvaluate() {
    }

    /**
     * Test of getSimpleAverage method, of class ADRManager.
     */
    @Test
    public void testGetSimpleAverage() {
    }

    /**
     * Test of getFirstRange method, of class ADRManager.
     */
    @Test
    public void testGetFirstRange() {
    }

    /**
     * Test of getSecondRange method, of class ADRManager.
     */
    @Test
    public void testGetSecondRange() {
    }

    @Test
    public void testGetADR() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        List<InstrumentPriceLog> instrumentPriceLogList = dBAccess.findInstrumentPriceLogBySymbol("AUD/USD", 1, date, 20);
        System.out.println(instrumentPriceLogList.size());
        if (!instrumentPriceLogList.isEmpty()) {
            for (InstrumentPriceLog instrumentPriceLog : instrumentPriceLogList) {
                System.out.println(instrumentPriceLog.getDate());
            }
//            ADRManager aDRManager = ADRManager.getInstance();
//            aDRManager.setPeriods(new int[]{5});
//            double test = aDRManager.evaluate(instrumentPriceLogList);
//            System.out.println(test);
        }
        
//        List<InstrumentPriceLog> instrumentPriceLogLists = dBAccess.findInstrumentPriceLogBySymbol("EUR/JPY", 1);
//        System.out.println(instrumentPriceLogLists.size());
//        if (!instrumentPriceLogLists.isEmpty()) {
//            ADRManager aDRManager = ADRManager.getInstance();
//            aDRManager.setPeriods(new int[]{5});
//            double test = aDRManager.evaluate(instrumentPriceLogLists);
//            System.out.println(test);
//        }
    }

}