/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.BollingerBand;
import com.granberg.model.Forex;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class BollingerTest {

    public BollingerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testListLength() {
        int day = 10;
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<Forex> list = dBAccess.findNForexBySymbol("EUR/JPY", 15);
        while (list.size() != day) {
            list.remove(0);
        }
    }

    /**
     * Test of getBollinger method, of class Bollinger.
     */
    @Test
    public void testGetBollinger() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();

        int count = 0;
        int end = 10;
        double multiplier = 2.00 / (10.00 + 1.00);
        System.out.println(multiplier);
        double SMA = 0.00;
        double SUM = 0.00;
        double EMA = 0.00;
        double HBAND = 0.00;
        double LBAND = 0.00;
        List<Forex> list = dBAccess.findNForexBySymbol("EUR/JPY", 15);
        List<Double> EMAList = new LinkedList<>();
        while (true) {
            for (int i = count; i < end; i++) {
                Forex test = list.get(i);
                System.out.println("CLOSE: " + test.getClose());
                SUM += test.getClose().doubleValue();
                if (count == 0) {
                    EMAList.add(test.getClose().doubleValue());
                }
            }
//            if (!list.isEmpty()) {
//                for (Forex forex : list) {
//                    System.out.println("Close: " + forex.getClose());
//                    SUM += forex.getClose().doubleValue();
//                }
//            }
            System.out.println("SUM: " + SUM);
            SMA = SUM / 10;
            System.out.println("SMA: " + SMA);
            if (count == 0) {
                EMA = SMA;
                EMAList.remove(9);
                EMAList.add(EMA);
            } else {
                Forex test1 = list.get(end - 1);
                EMA = multiplier * (test1.getClose().doubleValue() - EMA) + EMA;
                EMAList.remove(0);
                EMAList.add(EMA);
            }
            System.out.println("EMA: " + EMA);
            StandardDeviation deviation = new StandardDeviation();
            double[] EMAArray = new double[EMAList.size()];
            for (int i = 0; i < EMAArray.length; i++) {
                EMAArray[i] = EMAList.get(i);
                System.out.println("CLOSED: " + EMAList.get(i));
            }
//129.44
//129.82
//129.81
//129.81
//129.83
//129.80
//129.78
//129.77
//129.74
//129.76
            System.out.println("deviation: " + deviation.evaluate(EMAArray));
            System.out.println("deviation: " + deviation.evaluate(EMAArray, SMA));
            HBAND = EMA + (deviation.evaluate(EMAArray)) * 2.2;
            System.out.println("HBAND: " + HBAND);
            LBAND = EMA - (deviation.evaluate(EMAArray)) * 2.2;
            System.out.println("LBAND: " + LBAND);
            count++;
            end++;
            SUM = 0.00;
        }


//        double[] doubleArray = new double[9];
//        doubleArray[0] = 22.27;
//        doubleArray[1] = 22.19;
//        doubleArray[2] = 22.08;
//        doubleArray[3] = 22.17;
//        doubleArray[4] = 22.18;
//        doubleArray[5] = 22.13;
//        doubleArray[6] = 22.23;
//        doubleArray[7] = 22.43;
//        doubleArray[8] = 22.24;
//        StandardDeviation deviation = new StandardDeviation();
//        double test = 22.22 + (deviation.evaluate(doubleArray)) * 2.2;
//        System.out.println(test);
//        System.out.println(deviation.evaluate(doubleArray));
    }

    @Test
    public void testGetBollingerList() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<Forex> list = dBAccess.findNForexBySymbol("EUR/JPY", 15);
        BollingerManager bollingerManager = BollingerManager.getInstance();
        bollingerManager.setPeriod(10);
//        List<BollingerBand> bbs = bollingerManager.evaluate(list);
//        for (BollingerBand bollingerBand : bbs) {
//            System.out.println("SMA: " + bollingerBand.getSMA());
//            System.out.println("EMA: " + bollingerBand.getEMA());
//            System.out.println("HBAND: " + bollingerBand.getHBAND());
//            System.out.println("LBAND: " + bollingerBand.getLBAND());
//        }
    }
}