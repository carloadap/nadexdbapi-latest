/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.model.Contract;
import com.granberg.model.Forex;
import com.granberg.model.HourlyPips;
import com.granberg.model.InstrumentPriceLog;
import com.granberg.model.News;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccessTest {

    private IDBAccess dbAccess;

    public MySQLDBAccessTest() {
        //dbAccess =  Provider.getInstance().create(); //
        dbAccess = MySQLDBAccess.getInstance();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void findNewsByWithinPeriod() throws ParseException {
        //Paste to browser:
        // http://localhost:8080/NadexWS/webresources/news/findNewsByWithinPeriod/1372935600000/1372938300000
        // Should return 8 News

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.SSS");
        String startTime = "2013-07-04 07:00:00.000";
        String endTime = "2013-07-04 07:45:00.000";

        Date startDate = df.parse(startTime);
        System.out.println("StartTime long: " + startDate.getTime());

        Date endDate = df.parse(endTime);
        System.out.println("EndTime long: " + endDate.getTime());

        List<News> news = dbAccess.findNewsByWithinPeriod(startDate, endDate);

        System.out.println("NEWS SIZE: " + news.size());
        System.out.println("NEWS \n" + news);
    }

    @Test
    public void findNewsByCurrency() {
        List<News> newsList = dbAccess.findNewsByCurrency("USD/CAD");
        assertEquals(39, newsList.size());
    }

    /**
     * Test of findMarket method, of class MySQLDBAccess.
     */
    @Test
    public void testFindMarket() {
    }

    /**
     * Test of findInstrument method, of class MySQLDBAccess.
     */
    @Test
    public void testFindInstrument() {
    }

    /**
     * Test of findInstrumentType method, of class MySQLDBAccess.
     */
    @Test
    public void testFindInstrumentType() {
    }

    /**
     * Test of findOrderType method, of class MySQLDBAccess.
     */
    @Test
    public void testFindOrderType() {
    }

    /**
     * Test of findNews method, of class MySQLDBAccess.
     */
    @Test
    public void testFindNews() {
    }

    /**
     * Test of findContracts method, of class MySQLDBAccess.
     */
    @Test
    public void testFindContracts() {
    }

    /**
     * Test of findTickets method, of class MySQLDBAccess.
     */
    @Test
    public void testFindTickets() {
    }

    /**
     * Test of findPositions method, of class MySQLDBAccess.
     */
    @Test
    public void testFindPositions() {
    }

    /**
     * Test of findPeriodicity method, of class MySQLDBAccess.
     */
    @Test
    public void testFindPeriodicity() {
    }

    @Test
    public void testUpdateInstrumentPrice() {
        dbAccess.updateInstrumentPrice("EUR/JPY", BigDecimal.TEN);

    }

    @Test
    public void getNewsStartDateAndEndDate() {
        Date startDate = new Date();
        Calendar cal = Calendar.getInstance();
//            cal.setTime(dataObject.getNewsDate());
        cal.setTime(startDate);
        cal.add(Calendar.DATE, 4);
        Date endDate = cal.getTime();
        System.out.println("Start Date : " + startDate + "End Date : " + endDate);
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss zzz");
//            sendingDate = dateFormat.parse(sendingDate.toString());
//            System.out.println(sendingDate);
        List<News> list = dbAccess.findNewsByWithinPeriod(startDate, endDate);
        for (News news : list) {
            System.out.println(news.getEvents() + news.getDate());
        }
    }

    @Test
    public void findHourlyPipsByInstrumentNameAndAverageType() {
        List<HourlyPips> hps = dbAccess.getHourlyPips("AUD/USD", 7, 1);
        assertEquals(24, hps.size());
    }

    @Test
    public void findNearestNewsForSpecificInstrument() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        boolean haveNews = dbAccess.findNearestNewsForSpecificInstrument("EUR/JPY");
        System.out.println(haveNews);
    }

    @Test
    public void testBetweenTwoDates() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -60);
        Date startDate = calendar.getTime();
        System.out.println(startDate);
        calendar.add(Calendar.MINUTE, 60);
        Date endDate = calendar.getTime();
        System.out.println(endDate);
    }

    @Test
    public void testContract() {
        Contract contract = new Contract();
        if (contract.getContractSymbol() == null) {
            System.out.println("null");
        }
    }

    @Test
    public void testLog() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        Date date = dBAccess.instrumentHistoryLastLog("EUR/JPY");
//        InstrumentPriceLog instrumentHistory = dBAccess.findInstrumentHistoryByInstrumentName("EUR/JPY");
    }

    @Test
    public void getForexWithLimit() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<Forex> list = dBAccess.findNForexBySymbol("EUR/JPY", 10);
        if (!list.isEmpty()) {
            for (Forex forex : list) {
                System.out.println(String.format("%s , %s , %s", forex.getSymbol(), forex.getClose(), forex.getDate()));
            }
        }
    }

    @Test
    public void testGetAdrMaxDate() {
        Date date = dbAccess.findSessionAdrMaxDate(4);
        System.out.println(date);
    }

    @Test
    public void testGetContract() {
//        List<Contract> contractList = dbAccess.findContractByInstrumentIdAndActive(13);
//        System.out.println(contractList.size());
//        for (Contract contract : contractList) {
//            System.out.println(contract.getContractDescription());
//        }

        Contract contract = dbAccess.findContractBySymbol("NX.VS.OPT.SOY.D.1.1");
        System.out.println(contract.getExpirationDate());
    }

    @Test
    public void testDeleteContract() {
        Contract contract = new Contract();
        contract.setContractSymbol("ES");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        System.out.println(date);
        contract.setExpirationDate(date);
        dbAccess.deleteContract(contract);
    }

    @Test
    public void testUpdateContract() {
        Contract contract = new Contract();
        contract.setContractSymbol("BE");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        System.out.println(date);
        contract.setExpirationDate(date);
        contract.setSecurityId("HELLOX");
        contract.setInstrumentId(10);
//        dbAccess.updateContract(contract);
    }
}
