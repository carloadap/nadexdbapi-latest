/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.controller.ADRMappingJpaController;
import com.granberg.controller.AdrJpaController;
import com.granberg.controller.BiasJpaController;
import com.granberg.controller.MarketDataLogJpaController;
import com.granberg.controller.ContractJpaController;
import com.granberg.controller.ContractTypeJpaController;
import com.granberg.controller.DelayValueJpaController;
import com.granberg.controller.EntryOrderTypeJpaController;
import com.granberg.controller.ForexJpaController;
import com.granberg.controller.ForexPeriodJpaController;
import com.granberg.controller.HourlyPipsJpaController;
import com.granberg.controller.InstrumentJpaController;
import com.granberg.controller.InstrumentMappingJpaController;
import com.granberg.controller.InstrumentPriceLogJpaController;
import com.granberg.controller.InstrumentTypeJpaController;
import com.granberg.controller.MarketDataJpaController;
import com.granberg.controller.MarketJpaController;
import com.granberg.controller.MonitorTicketJpaController;
import com.granberg.controller.NadexContextJpaController;
import com.granberg.controller.NadexCurrencyJpaController;
import com.granberg.controller.NewsJpaController;
import com.granberg.controller.NewsRecipientExtension;
import com.granberg.controller.NewsRecipientJpaController;
import com.granberg.controller.OrderTypeJpaController;
import com.granberg.controller.PeriodicityJpaController;
import com.granberg.controller.PositionJpaController;
import com.granberg.controller.SecurityStatusJpaController;
import com.granberg.controller.SessionAdrJpaController;
import com.granberg.controller.SessionJpaController;
import com.granberg.controller.TicketJpaController;
import com.granberg.controller.TradeJpaController;
import com.granberg.controller.TradeMonitorJpaController;
import com.granberg.controller.TrendJpaController;
import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.controller.exceptions.PreexistingEntityException;
import com.granberg.model.ADRMapping;
import com.granberg.model.Adr;
import com.granberg.model.Bias;
import com.granberg.model.Contract;
import com.granberg.model.ContractType;
import com.granberg.model.DelayValue;
import com.granberg.model.EntryOrderType;
import com.granberg.model.Forex;
import com.granberg.model.ForexPeriod;
import com.granberg.model.HourlyPips;
import com.granberg.model.Instrument;
import com.granberg.model.InstrumentMapping;
import com.granberg.model.InstrumentPriceLog;
import com.granberg.model.InstrumentType;
import com.granberg.model.Market;
import com.granberg.model.MarketData;
import com.granberg.model.MarketDataLog;
import com.granberg.model.MonitorTicket;
import com.granberg.model.NadexContext;
import com.granberg.model.NadexCurrency;
import com.granberg.model.News;
import com.granberg.model.NewsRecipient;
import com.granberg.model.OrderType;
import com.granberg.model.Periodicity;
import com.granberg.model.Position;
import com.granberg.model.SecurityStatus;
import com.granberg.model.Session;
import com.granberg.model.SessionAdr;
import com.granberg.model.Ticket;
import com.granberg.model.Trade;
import com.granberg.model.TradeMonitor;
import com.granberg.model.Trend;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccess implements IDBAccess {

    public EntityManagerFactory emf;
    private static MySQLDBAccess instance;
    public static String PERSISTENCE_UNIT_NAME;

    /**
     * Make the constructor private
     */
    private MySQLDBAccess() {
        emf = Persistence.createEntityManagerFactory("NadexDBAPIPU");
//        emf.getCache().evictAll();
//        System.out.println("running");
        PERSISTENCE_UNIT_NAME = "NadexDBAPIPU";
    }

    /**
     * Get the Instance from static method
     *
     * @return
     */
    public static MySQLDBAccess getInstance() {
        if (instance == null) {
            instance = new MySQLDBAccess();
        }
        return instance;
    }

    @Override
    public List<Market> findMarket() {

        MarketJpaController marketController = new MarketJpaController(emf);
        return marketController.findMarketEntities();

    }

    @Override
    public List<Instrument> findInstrument() {
        InstrumentJpaController instrumentController = new InstrumentJpaController(emf);
        return instrumentController.findInstrumentEntities();
    }

    @Override
    public List<InstrumentType> findInstrumentType() {
        InstrumentTypeJpaController instrumentTypeController = new InstrumentTypeJpaController(emf);
        return instrumentTypeController.findInstrumentTypeEntities();
    }

    @Override
    public List<OrderType> findOrderType() {
        OrderTypeJpaController orderTypeController = new OrderTypeJpaController(emf);
        return orderTypeController.findOrderTypeEntities();
    }

    @Override
    public List<News> findNews() {
        NewsJpaController newsController = new NewsJpaController(emf);
        return newsController.findNewsEntities();
    }

    @Override
    public List<Contract> findContracts() {
        ContractJpaController contractController = new ContractJpaController(emf);
        return contractController.findContractEntities();
    }

    @Override
    public List<Ticket> findTickets() {
        TicketJpaController ticketController = new TicketJpaController(emf);
        return ticketController.findTicketEntities();
    }

    @Override
    public List<Position> findPositions() {
        PositionJpaController positionController = new PositionJpaController(emf);
        return positionController.findPositionEntities();
    }

    @Override
    public List<Periodicity> findPeriodicity() {
        PeriodicityJpaController periodicityController = new PeriodicityJpaController(emf);
        return periodicityController.findPeriodicityEntities();
    }

    @Override
    public List<News> findSameDateTimeNews(Date newsDate) {
        NewsJpaController newsJpaController = new NewsJpaController(emf);
        return newsJpaController.findNewsByDate(newsDate);
    }

    @Override
    public List<Date> findDistinctNews() {
        NewsJpaController newsJpaController = new NewsJpaController(emf);
        return newsJpaController.findUniqueDates();
    }

    @Override
    public DelayValue findDelayValue(int valueID) {
        DelayValueJpaController delayValueJpaController = new DelayValueJpaController(emf);
        return delayValueJpaController.findDelayValue(valueID);
    }

    @Override
    public int countNews() {
        NewsJpaController newsCount = new NewsJpaController(emf);
        return newsCount.getNewsCount();
    }

    @Override
    public List<News> findNewsByWithinPeriod(Date date, Date endDate) {
        NewsJpaController newsJpaController = new NewsJpaController(emf);
        return newsJpaController.findNewsByWithinPeriod(date, endDate);
    }

    @Override
    public List<String> findDistinctEvent() {
        NewsJpaController newsJpaController = new NewsJpaController(emf);
        return newsJpaController.findUniqueEvents();
    }

    @Override
    public List<NewsRecipient> findRecipients() {
        NewsRecipientJpaController recipientJpaController = new NewsRecipientJpaController(emf);
        return recipientJpaController.findNewsRecipientEntities();
    }

    @Override
    public List<NewsRecipient> findActiveRecipients(boolean active) {
        NewsRecipientExtension newsRecipientJpaController = new NewsRecipientExtension(emf);
        return newsRecipientJpaController.findActiveNewsRecipients(active);
    }

    @Override
    public void updateInstrumentPrice(String name, BigDecimal price) {
        //implement the code that will update the price
        InstrumentJpaController instrumentJpaController = new InstrumentJpaController(emf);
        Instrument instrument = instrumentJpaController.findByInstrumentName(name);
        instrument.setInstrumentPrice(price);
        instrument.setUpdatedOn(new Date());
        try {
            instrumentJpaController.edit(instrument);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<News> findNewsByCurrency(String currency) {
        NewsJpaController controller = new NewsJpaController(emf);
        return controller.findNewsByCurrency(currency);

    }

    @Override
    public void createSecurityStatus(SecurityStatus securityStatus) {
        SecurityStatusJpaController controller = new SecurityStatusJpaController(emf);
        controller.create(securityStatus);
    }

    @Override
    public ContractType findContractTypeByName(String name) {
        ContractTypeJpaController controller = new ContractTypeJpaController(emf);
        return controller.findContractTypeByName(name);
    }

    @Override
    public InstrumentMapping findInstrumentByAlternateName(String name) {
        InstrumentMappingJpaController controller = new InstrumentMappingJpaController(emf);
        return controller.findInstrumentByName(name);
    }

    @Override
    public Instrument findInstrumentByName(String name) {

        InstrumentJpaController controller = new InstrumentJpaController(emf);
        return controller.findByInstrumentName(name);
    }

    @Override
    public Periodicity findPeriodicityByNadexCode(String name) {
        PeriodicityJpaController controller = new PeriodicityJpaController(emf);
        return controller.findPeriodicityByNadexCode(name);
    }

    @Override
    public void createNadexContext(NadexContext nadexContext) {
        try {
            NadexContextJpaController controller = new NadexContextJpaController(emf);
            controller.create(nadexContext);
        } catch (PreexistingEntityException ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void createContract(Contract contract) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.create(contract);
    }

    @Override
    public void createMarketData(MarketData marketData, String namedQuery) {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        controller.createMarketData(marketData, namedQuery);
    }

    @Override
    public Contract findContractBySymbol(String symbol) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractTypeBySymbol(symbol);
    }

    @Override
    public Instrument findIntrumentById(int id) {
        InstrumentJpaController controller = new InstrumentJpaController(emf);
        return controller.findInstrument(id);
    }

    @Override
    public List<NadexCurrency> findNadexCurrencyContainsName(String name) {
        NadexCurrencyJpaController controller = new NadexCurrencyJpaController(emf);
        return controller.findNadexCurrencyContainsName(name);
    }

    @Override
    public List<Contract> findContractListBySymbol(String name) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractListBySymbol(name);
    }

    @Override
    public List<News> findSameNews(String currency, String events, String importance, Date date) {
        NewsJpaController controller = new NewsJpaController(emf);
        return controller.findSameNews(currency, events, importance, date);
    }

    @Override
    public void createNews(News news) {
        NewsJpaController controller = new NewsJpaController(emf);
        controller.create(news);
    }

    @Override
    public void createContractCurrencyOnly(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId, int periodicityId) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.createContractCurrencyOnly(contract, namedQuery, instrumentId, instrumentTypeId, periodicityId);
    }

    @Override
    public List<HourlyPips> getHourlyPips(String instrumentName, Integer movingAverageInDays, Integer hourlyInterval) {
        HourlyPipsJpaController controller = new HourlyPipsJpaController(emf);
        return controller.findHourlyPipsByInstrumentNameAndAverageType(instrumentName, movingAverageInDays, hourlyInterval);
    }

    @Override
    public void updateSettledContract(String contractSymbol) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.updateSettledContract(contractSymbol);
    }

    @Override
    public Contract findContractByIdNotExpired(int contractId, Date date) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findNotExpiredContract(contractId, date);
    }

    @Override
    public List<Contract> findContractListBySymbolAndExpirationDate(Contract[] contract) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractListBySymbolAndExpirationDate(contract);
    }

    @Override
    public void updateMarketData(MarketData marketData, int columnToUpdate) {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        controller.updateMarketData(marketData, columnToUpdate);
    }

    @Override
    public List<Contract> findContractByInstrumentIdAndActive(int instrumentId) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractListByInstrumentId(instrumentId);
    }

    @Override
    public void createMarketDataLog(MarketDataLog marketDataLog) {
        MarketDataLogJpaController controller = new MarketDataLogJpaController(emf);
        controller.create(marketDataLog);
    }

    @Override
    public List<MarketData> getMarketDataList() {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        return controller.findMarketDataEntities();
    }

    @Override
    public MarketData findMarketDataByContractId(int contractId) {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        return controller.findMarketDataByContractId(contractId);
    }

    @Override
    public Date getOldMarketDataLog(String symbol, int priceUpdated) {
        MarketDataLogJpaController controller = new MarketDataLogJpaController(emf);
        return controller.findMarketDataLogBySymbol(symbol, priceUpdated);
    }

    @Override
    public MarketData findMarketDataByContractSymbol(String contractSymbol) {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        return controller.findMarketDataByContractSymBol(contractSymbol);
    }

    @Override
    public void updateTrend(Trend trend) {
        TrendJpaController controller = new TrendJpaController(emf);
        try {
            controller.edit(trend);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Trend> findAllTrends() {
        TrendJpaController controller = new TrendJpaController(emf);
        return controller.findTrendEntities();
    }

    @Override
    public boolean findNearestNewsForSpecificInstrument(String instrumentName) {
        NewsJpaController controller = new NewsJpaController(emf);
        return controller.findNearestNewsForSpecificInstrument(instrumentName);
    }

    @Override
    public Contract findContractByStrikePriceAndExpirationDate(BigDecimal strikePrice, Date expirationDate) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractByStrikePriceAndExpirationDate(strikePrice, expirationDate);
    }

    @Override
    public List<Contract> findContractByStrikePrice(BigDecimal strikePrice) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractByStrikePrice(strikePrice);
    }

    @Override
    public void createContractCurrencyOnlyNoPeriodicity(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.createContractCurrencyOnly(contract, namedQuery, instrumentId, instrumentTypeId);
    }

    @Override
    public List<Contract> findContractByCotractTypeAndInstrumentId(int contractType, int instrumentId) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractByCotractTypeAndInstrumentIdTest(contractType, instrumentId);
    }

    @Override
    public InstrumentPriceLog findPriceLogLatestByInstrumentName(String instrumentName, int periodId, Date date) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findPriceLogLatestByInstrumentName(instrumentName, periodId, date);
    }

    @Override
    public void UpdateInstrumentHistoryByLastDateAndInstrumentName(String instrumentName, BigDecimal close, BigDecimal high, BigDecimal low) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        controller.UpdateInstrumentHistoryByLastDateAndInstrumentName(instrumentName, close, high, low);
    }

    @Override
    public void createInstrumentHistory(InstrumentPriceLog instrumentHistory) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        controller.create(instrumentHistory);
    }

    @Override
    public Date instrumentHistoryLastLog(String instrumentName) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findInstrumentHistoryLastDate(instrumentName);
    }

    @Override
    public List<Forex> findNForexBySymbol(String symbol, int limit) {
        ForexJpaController controller = new ForexJpaController(emf);
        return controller.findNForexBySymbol(symbol, limit);
    }

    @Override
    public List<Forex> findForexBySymbol(String symbol) {
        ForexJpaController controller = new ForexJpaController(emf);
        return controller.findForexBySymbol(symbol);
    }

    @Override
    public void createInstrumentPriceLog(InstrumentPriceLog[] instrumentPriceLog) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        controller.createContractCurrencyOnly(instrumentPriceLog);
    }

    @Override
    public List<InstrumentPriceLog> findInstrumentPriceLogBySymbol(String symbol, int periodId, Date date, int maxResult) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findInstrumentHistoryListByInstrumentName(symbol, periodId, date, maxResult);
    }

    @Override
    public List<InstrumentPriceLog> findInstrumentPriceLogBySymbol(String symbol, int periodId, Date date) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findInstrumentHistoryListByInstrumentName(symbol, periodId, date);
    }

    @Override
    public Contract findContractByStrikePriceAndPeriodicityID(BigDecimal strikePrice, int periodicityID) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractByStrikePriceAndPeriod(strikePrice, periodicityID);
    }

    @Override
    public ForexPeriod findForexPeriodById(int id) {
        ForexPeriodJpaController controller = new ForexPeriodJpaController(emf);
        return controller.findForexPeriod(id);
    }

    @Override
    public ForexPeriod findForexPeriodByCode(String code) {
        ForexPeriodJpaController controller = new ForexPeriodJpaController(emf);
        return controller.findForexPeriodByCode(code);
    }

    @Override
    public InstrumentPriceLog findInstrumentPriceLogByInsturmentNameAndClose(String symbol, int periodId, Date date) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findByInstrumentNameAndClose(symbol, periodId, date);
    }

    @Override
    public Adr findADRByInstrumentName(String instrumentName) {
        AdrJpaController controller = new AdrJpaController(emf);
        return controller.findInstrumentHistoryByInstrumentName(instrumentName);
    }
    
    @Override
    public Adr SelectSpecificAdr(String instrumentName, Date date, int sessionId) {
        AdrJpaController controller = new AdrJpaController(emf);
        return controller.findLatestRunningADR(instrumentName, date, sessionId);
    }

    @Override
    public List<Adr> SelectAllAdr(Date date, int sessionId) {
        AdrJpaController controller = new AdrJpaController(emf);
        return controller.findLatestRunningADR(date, sessionId);
    }

    @Override
    public void createADR(Adr[] adr) {
        AdrJpaController controller = new AdrJpaController(emf);
        controller.createArray(adr);
    }
    
    @Override
    public void createTrade(Trade[] trade) {
        TradeJpaController controller = new TradeJpaController(emf);
        controller.createArray(trade);
    }

    @Override
    public void updateAdrHighLow(Adr adr) {
        AdrJpaController controller = new AdrJpaController(emf);
        controller.updateAdrHighLow(adr);
    }

    @Override
    public ADRMapping findADRMappingByInstrumentName(String instrumentName) {
        ADRMappingJpaController controller = new ADRMappingJpaController(emf);
        return controller.findInstrumentHistoryByInstrumentName(instrumentName);
    }

    @Override
    public void updateAdr(Adr adr) {
        AdrJpaController controller = new AdrJpaController(emf);
        controller.updateAdr(adr);
    }

    @Override
    public Date findSessionAdrMaxDate(int sessionId) {
        AdrJpaController controller = new AdrJpaController(emf);
        return controller.findSessionLatestADR(sessionId);
    }
    
    @Override
    public boolean  findIfTradeExist(Date date, int sessionId){
        TradeJpaController controller = new TradeJpaController(emf);
        return controller.findIfTradeExist(date, sessionId);
    }

    @Override
    public Date findInstrumentMaxDate() {
        InstrumentJpaController controller = new InstrumentJpaController(emf);
        return controller.findLatestInstrumentDate();
    }

    @Override
    public Session findSessionById(int sessionId) {
        SessionJpaController controller = new SessionJpaController(emf);
        return controller.findSession(sessionId);
    }

    @Override
    public List<SessionAdr> findSessionAdrListByInstrumentNameAndSessionId(String instrumentName, int sessionId) {
        SessionAdrJpaController controller = new SessionAdrJpaController(emf);
        return controller.findInstrumentHistoryListByInstrumentName(instrumentName, sessionId);
    }

    @Override
    public Date findAdrMaxDate() {
        AdrJpaController controller = new AdrJpaController(emf);
        return controller.findLatestADR();
    }

    @Override
    public void createFXCMPriceLog(InstrumentPriceLog[] instrumentPriceLog) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        controller.createPriceLogList(instrumentPriceLog);
    }

    @Override
    public void updateFXCMPriceLog(InstrumentPriceLog[] instrumentPriceLog) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        controller.updatePriceLogList(instrumentPriceLog);
    }

    @Override
    public Date findInstrumentPriceLogMaxDate() {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findLatestInstrumentPriceLog();
    }

    @Override
    public Date findInstrumentPriceLogMaxDateByPeriodType(int periodId) {
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findLatestInstrumentPriceLogByPeriodId(periodId);
    }

    @Override
    public void createContractBullSpreadOnly(Contract[] contract, int contractTypeId) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.createContractBullSpreadOnly(contract, contractTypeId);
    }

    @Override
    public List<MarketData> findMarketDataBidOfferEntryNotNull() {
        MarketDataJpaController controller = new MarketDataJpaController(emf);
        return controller.findMarketDataBidOffetNotNull();
    }

    @Override
    public List<MarketData> findMarketDataBidOfferEntryNotNullByContractSymbol(String contractSymbol) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createTicket(Ticket[] ticket) {
        TicketJpaController controller = new TicketJpaController(emf);
        controller.createTicket(ticket);
    }

    @Override
    public void createMonitorTicket(MonitorTicket[] monitorTicket, Date date) {
        MonitorTicketJpaController controller = new MonitorTicketJpaController(emf);
        controller.createMonitorTicket(monitorTicket, date);
    }

    @Override
    public MonitorTicket findMonitorTicketByTicketId(int ticketId) {
        MonitorTicketJpaController controller = new MonitorTicketJpaController(emf);
        return controller.findMonitorTicket(ticketId);
    }

    @Override
    public List<MonitorTicket> findAllMonitorTickets() {
        MonitorTicketJpaController controller = new MonitorTicketJpaController(emf);
        return controller.findMonitorTicketEntities();
    }

    @Override
    public Ticket findTicketbyTicketId(int ticketId) {
        TicketJpaController controller = new TicketJpaController(emf);
        return controller.findTicket(ticketId);
    }

    @Override
    public void updateMonitorTicket(MonitorTicket monitorTicket, Instrument instrument, Date date) {
        MonitorTicketJpaController controller = new MonitorTicketJpaController(emf);
        controller.updateMonitorTicket(monitorTicket, instrument, date);
    }

    @Override
    public Ticket findTicketByContractId(int contractId) {
        TicketJpaController controller = new TicketJpaController(emf);
        return controller.findTicketByContractId(contractId);
    }

    @Override
    public Contract findContractById(int contractId) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContract(contractId);
    }

    @Override
    public void updateMonitorTicketTest(MonitorTicket monitorTicket, MarketData marketData, Ticket ticket, Date date, double multiplier) {
        MonitorTicketJpaController controller = new MonitorTicketJpaController(emf);
        controller.updateMonitorTicketTest(monitorTicket, marketData, ticket, date, multiplier);
    }

    @Override
    public List<Ticket> findAllTickets() {
        TicketJpaController controller = new TicketJpaController(emf);
        return controller.findTicketEntities();
    }

    @Override
    public void updateTicket(Ticket[] ticket) {
        TicketJpaController controller = new TicketJpaController(emf);
        controller.updateTicket(ticket);
    }

    @Override
    public List<Ticket> findAllTicketsNotExpired(String statusId) {
        TicketJpaController controller = new TicketJpaController(emf);
        return controller.findAllTicketNotExpired(statusId);
    }

    @Override
    public void updateTicket(Ticket ticket) {
        try {
            TicketJpaController controller = new TicketJpaController(emf);
            controller.edit(ticket);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Contract findContractBySymbolNotExpired(String contractSymbol, Date date) {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findNotExpiredContractBySymbol(contractSymbol, date);
    }

    @Override
    public List<Contract> findContractListNotExpired() {
        ContractJpaController controller = new ContractJpaController(emf);
        return controller.findContractNotExpired();
    }

    @Override
    public Bias findBiasByEmail(String email) {
        BiasJpaController controller = new BiasJpaController(emf);
        return controller.findBiasByEmail(email);
    }

    @Override
    public void deleteContract(Contract contract) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.deleteContract(contract);
    }

    @Override
    public void updateContract(Contract contract, int contractTypeId) {
        ContractJpaController controller = new ContractJpaController(emf);
        controller.updateCurrentContract(contract, contractTypeId);
    }
    
    @Override
    public List<InstrumentPriceLog> findByInstrumentNameAndPeriodType(String instrumentName, int periodId){
        InstrumentPriceLogJpaController controller = new InstrumentPriceLogJpaController(emf);
        return controller.findByInstrumentNameAndPeriodType(instrumentName, periodId);
    }

    @Override
    public EntryOrderType findEntryOrderTypeByEntryTypeId(int entryTypeId) {
        EntryOrderTypeJpaController controller = new EntryOrderTypeJpaController(emf);
        return controller.findEntryOrderType(entryTypeId);
    }

    @Override
    public void createTradeMonitor(TradeMonitor[] tradeMonitorList) {
        TradeMonitorJpaController controller = new TradeMonitorJpaController(emf);
        controller.createArray(tradeMonitorList);
    }

    @Override
    public boolean findIfTradeMonitorExist(Date date) {
        TradeMonitorJpaController controller = new TradeMonitorJpaController(emf);
        return controller.findIfTradeMonitorExist(date);
    }

    @Override
    public List<TradeMonitor> findAllTradeMonitor() {
        TradeMonitorJpaController controller = new TradeMonitorJpaController(emf);
        return controller.findTradeMonitorEntities();
    }

    @Override
    public void updateTradeMonitor(TradeMonitor tradeMonitor) {
        try {
            TradeMonitorJpaController controller = new TradeMonitorJpaController(emf);
            controller.edit(tradeMonitor);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MySQLDBAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
