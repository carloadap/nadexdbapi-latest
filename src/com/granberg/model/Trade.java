/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "trade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trade.findAll", query = "SELECT t FROM Trade t"),
    @NamedQuery(name = "Trade.findByTradeId", query = "SELECT t FROM Trade t WHERE t.tradeId = :tradeId"),
    @NamedQuery(name = "Trade.findByCurrency", query = "SELECT t FROM Trade t WHERE t.currency = :currency"),
    @NamedQuery(name = "Trade.findByType", query = "SELECT t FROM Trade t WHERE t.type = :type"),
    @NamedQuery(name = "Trade.findByPrice", query = "SELECT t FROM Trade t WHERE t.price = :price"),
    @NamedQuery(name = "Trade.findByTakeProfit", query = "SELECT t FROM Trade t WHERE t.takeProfit = :takeProfit"),
    @NamedQuery(name = "Trade.findByStopLoss", query = "SELECT t FROM Trade t WHERE t.stopLoss = :stopLoss"),
    @NamedQuery(name = "Trade.findBySessionId", query = "SELECT t FROM Trade t WHERE t.sessionId = :sessionId"),
    @NamedQuery(name = "Trade.findBySessionIdAndDate", query = "SELECT t FROM Trade t WHERE t.date =:date AND t.sessionId = :sessionId"),
    @NamedQuery(name = "Trade.findByDate", query = "SELECT t FROM Trade t WHERE t.date = :date")})
public class Trade implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trade_id")
    private Integer tradeId;
    @Column(name = "currency")
    private String currency;
    @Column(name = "type")
    private String type;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "take_profit")
    private BigDecimal takeProfit;
    @Column(name = "stop_loss")
    private BigDecimal stopLoss;
    @Column(name = "session_id")
    private Integer sessionId;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Trade() {
    }

    public Trade(Integer tradeId) {
        this.tradeId = tradeId;
    }

    public Integer getTradeId() {
        return tradeId;
    }

    public void setTradeId(Integer tradeId) {
        this.tradeId = tradeId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTakeProfit() {
        return takeProfit;
    }

    public void setTakeProfit(BigDecimal takeProfit) {
        this.takeProfit = takeProfit;
    }

    public BigDecimal getStopLoss() {
        return stopLoss;
    }

    public void setStopLoss(BigDecimal stopLoss) {
        this.stopLoss = stopLoss;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tradeId != null ? tradeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trade)) {
            return false;
        }
        Trade other = (Trade) object;
        if ((this.tradeId == null && other.tradeId != null) || (this.tradeId != null && !this.tradeId.equals(other.tradeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Trade[ tradeId=" + tradeId + " ]";
    }
    
}
