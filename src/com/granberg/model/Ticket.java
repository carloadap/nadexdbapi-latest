/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Ticket")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ticket.findAll", query = "SELECT t FROM Ticket t"),
    @NamedQuery(name = "Ticket.findByTicketId", query = "SELECT t FROM Ticket t WHERE t.ticketId = :ticketId"),
    @NamedQuery(name = "Ticket.findByContractId", query = "SELECT t FROM Ticket t WHERE t.contractId = :contractId"),
    @NamedQuery(name = "Ticket.findByPositionSize", query = "SELECT t FROM Ticket t WHERE t.positionSize = :positionSize"),
    @NamedQuery(name = "Ticket.findByPositionId", query = "SELECT t FROM Ticket t WHERE t.positionId = :positionId"),
    @NamedQuery(name = "Ticket.findByPrice", query = "SELECT t FROM Ticket t WHERE t.price = :price"),
    @NamedQuery(name = "Ticket.findByMaxLoss", query = "SELECT t FROM Ticket t WHERE t.maxLoss = :maxLoss"),
    @NamedQuery(name = "Ticket.findByMaxProfit", query = "SELECT t FROM Ticket t WHERE t.maxProfit = :maxProfit"),
    @NamedQuery(name = "Ticket.findByProfitTarget", query = "SELECT t FROM Ticket t WHERE t.profitTarget = :profitTarget"),
    @NamedQuery(name = "Ticket.findByCreatedOn", query = "SELECT t FROM Ticket t WHERE t.createdOn = :createdOn"),
    @NamedQuery(name = "Ticket.findByUpdatedOn", query = "SELECT t FROM Ticket t WHERE t.updatedOn = :updatedOn"),
    @NamedQuery(name = "Ticket.findByStatus", query = "SELECT t FROM Ticket t WHERE t.status = :status")})
public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ticket_id")
    private Integer ticketId;
    @Basic(optional = false)
    @Column(name = "contract_id")
    private int contractId;
    @Basic(optional = false)
    @Column(name = "position_size")
    private int positionSize;
    @Basic(optional = false)
    @Column(name = "position_id")
    private int positionId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "max_loss")
    private BigDecimal maxLoss;
    @Column(name = "max_profit")
    private BigDecimal maxProfit;
    @Column(name = "profit_target")
    private BigDecimal profitTarget;
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @Column(name = "status")
    private String status;

    public Ticket() {
    }

    public Ticket(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Ticket(Integer ticketId, int contractId, int positionSize, int positionId) {
        this.ticketId = ticketId;
        this.contractId = contractId;
        this.positionSize = positionSize;
        this.positionId = positionId;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }

    public int getPositionSize() {
        return positionSize;
    }

    public void setPositionSize(int positionSize) {
        this.positionSize = positionSize;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMaxLoss() {
        return maxLoss;
    }

    public void setMaxLoss(BigDecimal maxLoss) {
        this.maxLoss = maxLoss;
    }

    public BigDecimal getMaxProfit() {
        return maxProfit;
    }

    public void setMaxProfit(BigDecimal maxProfit) {
        this.maxProfit = maxProfit;
    }

    public BigDecimal getProfitTarget() {
        return profitTarget;
    }

    public void setProfitTarget(BigDecimal profitTarget) {
        this.profitTarget = profitTarget;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ticketId != null ? ticketId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ticket)) {
            return false;
        }
        Ticket other = (Ticket) object;
        if ((this.ticketId == null && other.ticketId != null) || (this.ticketId != null && !this.ticketId.equals(other.ticketId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Ticket[ ticketId=" + ticketId + " ]";
    }
    
}
