/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "session_adr")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SessionAdr.findAll", query = "SELECT s FROM SessionAdr s"),
    @NamedQuery(name = "SessionAdr.findBySessionAdrId", query = "SELECT s FROM SessionAdr s WHERE s.sessionAdrId = :sessionAdrId"),
    @NamedQuery(name = "SessionAdr.findBySessionId", query = "SELECT s FROM SessionAdr s WHERE s.sessionId = :sessionId"),
    @NamedQuery(name = "SessionAdr.findByInstrumentNameAndSessionId", query = "SELECT s FROM SessionAdr s WHERE s.instrumentName = :instrumentName AND s.sessionId = :sessionId ORDER BY s.date asc"),
    @NamedQuery(name = "SessionAdr.findByInstrumentName", query = "SELECT s FROM SessionAdr s WHERE s.instrumentName = :instrumentName"),
    @NamedQuery(name = "SessionAdr.findByCurrentHigh", query = "SELECT s FROM SessionAdr s WHERE s.currentHigh = :currentHigh"),
    @NamedQuery(name = "SessionAdr.findByCurrentLow", query = "SELECT s FROM SessionAdr s WHERE s.currentLow = :currentLow"),
    @NamedQuery(name = "SessionAdr.findByDate", query = "SELECT s FROM SessionAdr s WHERE s.date = :date")})
public class SessionAdr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "session_adr_id")
    private Integer sessionAdrId;
    @Column(name = "session_id")
    private Integer sessionId;
    @Column(name = "instrument_name")
    private String instrumentName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "current_high")
    private BigDecimal currentHigh;
    @Column(name = "current_low")
    private BigDecimal currentLow;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    public SessionAdr() {
    }

    public SessionAdr(Integer sessionAdrId) {
        this.sessionAdrId = sessionAdrId;
    }

    public Integer getSessionAdrId() {
        return sessionAdrId;
    }

    public void setSessionAdrId(Integer sessionAdrId) {
        this.sessionAdrId = sessionAdrId;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public BigDecimal getCurrentHigh() {
        return currentHigh;
    }

    public void setCurrentHigh(BigDecimal currentHigh) {
        this.currentHigh = currentHigh;
    }

    public BigDecimal getCurrentLow() {
        return currentLow;
    }

    public void setCurrentLow(BigDecimal currentLow) {
        this.currentLow = currentLow;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessionAdrId != null ? sessionAdrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SessionAdr)) {
            return false;
        }
        SessionAdr other = (SessionAdr) object;
        if ((this.sessionAdrId == null && other.sessionAdrId != null) || (this.sessionAdrId != null && !this.sessionAdrId.equals(other.sessionAdrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.SessionAdr[ sessionAdrId=" + sessionAdrId + " ]";
    }
    
}
