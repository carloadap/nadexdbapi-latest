/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "NadexCurrency")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NadexCurrency.findAll", query = "SELECT n FROM NadexCurrency n"),
    @NamedQuery(name = "NadexCurrency.findByNadexId", query = "SELECT n FROM NadexCurrency n WHERE n.nadexId = :nadexId"),
    @NamedQuery(name = "NadexCurrency.findByContainsName", query = "SELECT n FROM NadexCurrency n WHERE n.name LIKE :name"),
    @NamedQuery(name = "NadexCurrency.findByName", query = "SELECT n FROM NadexCurrency n WHERE n.name = :name")})
public class NadexCurrency implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "nadex_id")
    private Integer nadexId;
    @Column(name = "name")
    private String name;

    public NadexCurrency() {
    }

    public NadexCurrency(Integer nadexId) {
        this.nadexId = nadexId;
    }

    public Integer getNadexId() {
        return nadexId;
    }

    public void setNadexId(Integer nadexId) {
        this.nadexId = nadexId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nadexId != null ? nadexId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NadexCurrency)) {
            return false;
        }
        NadexCurrency other = (NadexCurrency) object;
        if ((this.nadexId == null && other.nadexId != null) || (this.nadexId != null && !this.nadexId.equals(other.nadexId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.NadexCurrency[ nadexId=" + nadexId + " ]";
    }
}
