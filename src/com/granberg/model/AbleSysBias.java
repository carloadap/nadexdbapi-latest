/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "AbleSysBias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AbleSysBias.findAll", query = "SELECT a FROM AbleSysBias a"),
    @NamedQuery(name = "AbleSysBias.findByBiasId", query = "SELECT a FROM AbleSysBias a WHERE a.biasId = :biasId"),
    @NamedQuery(name = "AbleSysBias.findByInstrumentName", query = "SELECT a FROM AbleSysBias a WHERE a.instrumentName = :instrumentName"),
    @NamedQuery(name = "AbleSysBias.findByTrend", query = "SELECT a FROM AbleSysBias a WHERE a.trend = :trend"),
    @NamedQuery(name = "AbleSysBias.findByPrice", query = "SELECT a FROM AbleSysBias a WHERE a.price = :price"),
    @NamedQuery(name = "AbleSysBias.findByTimeFrame", query = "SELECT a FROM AbleSysBias a WHERE a.timeFrame = :timeFrame"),
    @NamedQuery(name = "AbleSysBias.findByDate", query = "SELECT a FROM AbleSysBias a WHERE a.date = :date")})
public class AbleSysBias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bias_id")
    private Integer biasId;
    @Column(name = "instrument_name")
    private String instrumentName;
    @Column(name = "trend")
    private String trend;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "time_frame")
    private String timeFrame;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public AbleSysBias() {
    }

    public AbleSysBias(Integer biasId) {
        this.biasId = biasId;
    }

    public Integer getBiasId() {
        return biasId;
    }

    public void setBiasId(Integer biasId) {
        this.biasId = biasId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (biasId != null ? biasId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AbleSysBias)) {
            return false;
        }
        AbleSysBias other = (AbleSysBias) object;
        if ((this.biasId == null && other.biasId != null) || (this.biasId != null && !this.biasId.equals(other.biasId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.AbleSysBias[ biasId=" + biasId + " ]";
    }
    
}
