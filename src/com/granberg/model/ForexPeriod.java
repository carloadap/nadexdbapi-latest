/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "forex_period")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ForexPeriod.findAll", query = "SELECT f FROM ForexPeriod f"),
    @NamedQuery(name = "ForexPeriod.findByForexPeriodId", query = "SELECT f FROM ForexPeriod f WHERE f.forexPeriodId = :forexPeriodId"),
    @NamedQuery(name = "ForexPeriod.findByCode", query = "SELECT f FROM ForexPeriod f WHERE f.code = :code"),
    @NamedQuery(name = "ForexPeriod.findByDescription", query = "SELECT f FROM ForexPeriod f WHERE f.description = :description")})
public class ForexPeriod implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "forex_period_id")
    private Integer forexPeriodId;
    @Column(name = "code")
    private String code;
    @Column(name = "description")
    private String description;

    public ForexPeriod() {
    }

    public ForexPeriod(Integer forexPeriodId) {
        this.forexPeriodId = forexPeriodId;
    }

    public Integer getForexPeriodId() {
        return forexPeriodId;
    }

    public void setForexPeriodId(Integer forexPeriodId) {
        this.forexPeriodId = forexPeriodId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (forexPeriodId != null ? forexPeriodId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ForexPeriod)) {
            return false;
        }
        ForexPeriod other = (ForexPeriod) object;
        if ((this.forexPeriodId == null && other.forexPeriodId != null) || (this.forexPeriodId != null && !this.forexPeriodId.equals(other.forexPeriodId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.ForexPeriod[ forexPeriodId=" + forexPeriodId + " ]";
    }
    
}
