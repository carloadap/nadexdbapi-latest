/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "ADRMapping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ADRMapping.findAll", query = "SELECT a FROM ADRMapping a"),
    @NamedQuery(name = "ADRMapping.findByAdrMappingId", query = "SELECT a FROM ADRMapping a WHERE a.adrMappingId = :adrMappingId"),
    @NamedQuery(name = "ADRMapping.findByInstrumentName", query = "SELECT a FROM ADRMapping a WHERE a.instrumentName = :instrumentName"),
    @NamedQuery(name = "ADRMapping.findByMultiplier", query = "SELECT a FROM ADRMapping a WHERE a.multiplier = :multiplier")})
public class ADRMapping implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "adr_mapping_id")
    private Integer adrMappingId;
    @Column(name = "instrument_name")
    private String instrumentName;
    @Column(name = "multiplier")
    private Integer multiplier;

    public ADRMapping() {
    }

    public ADRMapping(Integer adrMappingId) {
        this.adrMappingId = adrMappingId;
    }

    public Integer getAdrMappingId() {
        return adrMappingId;
    }

    public void setAdrMappingId(Integer adrMappingId) {
        this.adrMappingId = adrMappingId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adrMappingId != null ? adrMappingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ADRMapping)) {
            return false;
        }
        ADRMapping other = (ADRMapping) object;
        if ((this.adrMappingId == null && other.adrMappingId != null) || (this.adrMappingId != null && !this.adrMappingId.equals(other.adrMappingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.ADRMapping[ adrMappingId=" + adrMappingId + " ]";
    }
    
}
