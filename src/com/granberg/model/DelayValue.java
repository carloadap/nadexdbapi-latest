/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Delay_Value")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DelayValue.findAll", query = "SELECT d FROM DelayValue d"),
    @NamedQuery(name = "DelayValue.findByDelayId", query = "SELECT d FROM DelayValue d WHERE d.delayId = :delayId"),
    @NamedQuery(name = "DelayValue.findByDelayValue", query = "SELECT d FROM DelayValue d WHERE d.delayValue = :delayValue")})
public class DelayValue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "delay_id")
    private Integer delayId;
    @Column(name = "delay_value")
    private Integer delayValue;

    public DelayValue() {
    }

    public DelayValue(Integer delayId) {
        this.delayId = delayId;
    }

    public Integer getDelayId() {
        return delayId;
    }

    public void setDelayId(Integer delayId) {
        this.delayId = delayId;
    }

    public Integer getDelayValue() {
        return delayValue;
    }

    public void setDelayValue(Integer delayValue) {
        this.delayValue = delayValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (delayId != null ? delayId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DelayValue)) {
            return false;
        }
        DelayValue other = (DelayValue) object;
        if ((this.delayId == null && other.delayId != null) || (this.delayId != null && !this.delayId.equals(other.delayId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.DelayValue[ delayId=" + delayId + " ]";
    }
    
}
