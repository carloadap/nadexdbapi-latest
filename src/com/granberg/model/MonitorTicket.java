/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "monitor_ticket")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MonitorTicket.findAll", query = "SELECT m FROM MonitorTicket m"),
    @NamedQuery(name = "MonitorTicket.findByMonitorId", query = "SELECT m FROM MonitorTicket m WHERE m.monitorId = :monitorId"),
    @NamedQuery(name = "MonitorTicket.findByTicketId", query = "SELECT m FROM MonitorTicket m WHERE m.ticketId = :ticketId"),
    @NamedQuery(name = "MonitorTicket.findByHighestPrice", query = "SELECT m FROM MonitorTicket m WHERE m.highestPrice = :highestPrice"),
    @NamedQuery(name = "MonitorTicket.findByHighestPriceDate", query = "SELECT m FROM MonitorTicket m WHERE m.highestPriceDate = :highestPriceDate"),
    @NamedQuery(name = "MonitorTicket.findByLowestPrice", query = "SELECT m FROM MonitorTicket m WHERE m.lowestPrice = :lowestPrice"),
    @NamedQuery(name = "MonitorTicket.findByLowestPriceDate", query = "SELECT m FROM MonitorTicket m WHERE m.lowestPriceDate = :lowestPriceDate"),
    @NamedQuery(name = "MonitorTicket.findByCurrentPrice", query = "SELECT m FROM MonitorTicket m WHERE m.currentPrice = :currentPrice"),
    @NamedQuery(name = "MonitorTicket.findByTotalProfit", query = "SELECT m FROM MonitorTicket m WHERE m.totalProfit = :totalProfit"),
    @NamedQuery(name = "MonitorTicket.findByUpdatedOn", query = "SELECT m FROM MonitorTicket m WHERE m.updatedOn = :updatedOn"),
    @NamedQuery(name = "MonitorTicket.findByCreatedOn", query = "SELECT m FROM MonitorTicket m WHERE m.createdOn = :createdOn")})
public class MonitorTicket implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "monitor_id")
    private Integer monitorId;
    @Column(name = "ticket_id")
    private Integer ticketId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "highest_price")
    private BigDecimal highestPrice;
    @Column(name = "highest_price_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date highestPriceDate;
    @Column(name = "lowest_price")
    private BigDecimal lowestPrice;
    @Column(name = "lowest_price_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lowestPriceDate;
    @Column(name = "current_price")
    private BigDecimal currentPrice;
    @Column(name = "total_profit")
    private BigDecimal totalProfit;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public MonitorTicket() {
    }

    public MonitorTicket(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public BigDecimal getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(BigDecimal highestPrice) {
        this.highestPrice = highestPrice;
    }

    public Date getHighestPriceDate() {
        return highestPriceDate;
    }

    public void setHighestPriceDate(Date highestPriceDate) {
        this.highestPriceDate = highestPriceDate;
    }

    public BigDecimal getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(BigDecimal lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public Date getLowestPriceDate() {
        return lowestPriceDate;
    }

    public void setLowestPriceDate(Date lowestPriceDate) {
        this.lowestPriceDate = lowestPriceDate;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (monitorId != null ? monitorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MonitorTicket)) {
            return false;
        }
        MonitorTicket other = (MonitorTicket) object;
        if ((this.monitorId == null && other.monitorId != null) || (this.monitorId != null && !this.monitorId.equals(other.monitorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.MonitorTicket[ monitorId=" + monitorId + " ]";
    }
    
}
