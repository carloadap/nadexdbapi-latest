/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "InstrumentPriceLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstrumentPriceLog.findAll", query = "SELECT i FROM InstrumentPriceLog i"),
    @NamedQuery(name = "InstrumentPriceLog.findByInsturmentPriceLogId", query = "SELECT i FROM InstrumentPriceLog i WHERE i.insturmentPriceLogId = :insturmentPriceLogId"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentName", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndPeriodType", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.periodType = :periodType"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndPeriodId", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.periodType = :periodType AND i.date < :date ORDER BY i.date asc"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndPeriodIdDesc", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.periodType = :periodType AND i.date < :date ORDER BY i.date desc"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndDate", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.periodType = :periodType AND i.date = :date"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndLastDate", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.date = :date"),
    @NamedQuery(name = "InstrumentPriceLog.findByInstrumentNameAndLastDateAndPeriod", query = "SELECT i FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.date = :date AND i.periodType = :periodType"),
    @NamedQuery(name = "InstrumentPriceLog.findByOpen", query = "SELECT i FROM InstrumentPriceLog i WHERE i.open = :open"),
    @NamedQuery(name = "InstrumentPriceLog.findByHigh", query = "SELECT i FROM InstrumentPriceLog i WHERE i.high = :high"),
    @NamedQuery(name = "InstrumentPriceLog.findByLow", query = "SELECT i FROM InstrumentPriceLog i WHERE i.low = :low"),
    @NamedQuery(name = "InstrumentPriceLog.findByClose", query = "SELECT i FROM InstrumentPriceLog i WHERE i.close = :close"),
    @NamedQuery(name = "InstrumentPriceLog.findByPeriodType", query = "SELECT i FROM InstrumentPriceLog i WHERE i.periodType = :periodType"),
    @NamedQuery(name = "InstrumentPriceLog.findByLastDate", query = "SELECT MAX(i.date) FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName"),
    @NamedQuery(name = "InstrumentPriceLog.findByLastDateAndPeriod", query = "SELECT MAX(i.date) FROM InstrumentPriceLog i WHERE i.instrumentName = :instrumentName AND i.periodType = :periodType"),
    @NamedQuery(name = "InstrumentPriceLog.findLastDateByPeriod", query = "SELECT DISTINCT MAX(i.date) FROM InstrumentPriceLog i WHERE i.periodType = :periodType"),
    @NamedQuery(name = "InstrumentPriceLog.findByLastDateData", query = "SELECT DISTINCT MAX(i.date) FROM InstrumentPriceLog i"),
    @NamedQuery(name = "InstrumentPriceLog.findByDate", query = "SELECT i FROM InstrumentPriceLog i WHERE i.date = :date")})


public class InstrumentPriceLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "insturment_price_log_id")
    private Integer insturmentPriceLogId;
    @Column(name = "instrument_name")
    private String instrumentName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "open")
    private BigDecimal open;
    @Column(name = "high")
    private BigDecimal high;
    @Column(name = "low")
    private BigDecimal low;
    @Column(name = "close")
    private BigDecimal close;
    @Column(name = "period_type")
    private Integer periodType;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public InstrumentPriceLog() {
    }

    public InstrumentPriceLog(Integer insturmentPriceLogId) {
        this.insturmentPriceLogId = insturmentPriceLogId;
    }

    public Integer getInsturmentPriceLogId() {
        return insturmentPriceLogId;
    }

    public void setInsturmentPriceLogId(Integer insturmentPriceLogId) {
        this.insturmentPriceLogId = insturmentPriceLogId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public BigDecimal getOpen() {
        return open;
    }

    public void setOpen(BigDecimal open) {
        this.open = open;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getClose() {
        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public Integer getPeriodType() {
        return periodType;
    }

    public void setPeriodType(Integer periodType) {
        this.periodType = periodType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insturmentPriceLogId != null ? insturmentPriceLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrumentPriceLog)) {
            return false;
        }
        InstrumentPriceLog other = (InstrumentPriceLog) object;
        if ((this.insturmentPriceLogId == null && other.insturmentPriceLogId != null) || (this.insturmentPriceLogId != null && !this.insturmentPriceLogId.equals(other.insturmentPriceLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.InstrumentPriceLog[ insturmentPriceLogId=" + insturmentPriceLogId + " ]";
    }
    
}
