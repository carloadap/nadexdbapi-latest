/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@XmlRootElement(name = "EmailNotification")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailNotificationKey {

    @XmlElement(name = "instrumentName")
    private String instrumentName;

    /**
     * @return the instrumentName
     */
    public String getInstrumentName() {
        return instrumentName;
    }

    /**
     * @param instrumentName the instrumentName to set
     */
    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }
}
