/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "MarketData")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarketData.findAll", query = "SELECT m FROM MarketData m"),
    @NamedQuery(name = "MarketData.findByMarketDataId", query = "SELECT m FROM MarketData m WHERE m.marketDataId = :marketDataId"),
    @NamedQuery(name = "MarketData.findByContractId", query = "SELECT m FROM MarketData m WHERE m.contractId = :contractId"),
    @NamedQuery(name = "MarketData.findBySymbol", query = "SELECT m FROM MarketData m WHERE m.symbol = :symbol"),
    @NamedQuery(name = "MarketData.findBySymbolSortByContractId", query = "SELECT m FROM MarketData m WHERE m.symbol = :symbol ORDER BY m.contractId DESC"),
    @NamedQuery(name = "MarketData.findByBid", query = "SELECT m FROM MarketData m WHERE m.bid = :bid"),
    @NamedQuery(name = "MarketData.findAllBidOfferEntryNotNull", query = "SELECT m FROM MarketData m WHERE m.bid is not null AND m.offer is not null AND m.entrySize is not null"),
    @NamedQuery(name = "MarketData.findByOffer", query = "SELECT m FROM MarketData m WHERE m.offer = :offer"),
    @NamedQuery(name = "MarketData.findByEntrySize", query = "SELECT m FROM MarketData m WHERE m.entrySize = :entrySize"),
    @NamedQuery(name = "MarketData.findBySendingTime", query = "SELECT m FROM MarketData m WHERE m.sendingTime = :sendingTime")})
public class MarketData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "market_data_id")
    private Integer marketDataId;
    @Column(name = "contract_id")
    private Integer contractId;
    @Column(name = "symbol")
    private String symbol;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "bid")
    private BigDecimal bid;
    @Column(name = "offer")
    private BigDecimal offer;
    @Column(name = "entry_size")
    private Integer entrySize;
    @Column(name = "sending_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendingTime;

    public MarketData() {
    }

    public MarketData(Integer marketDataId) {
        this.marketDataId = marketDataId;
    }

    public Integer getMarketDataId() {
        return marketDataId;
    }

    public void setMarketDataId(Integer marketDataId) {
        this.marketDataId = marketDataId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public BigDecimal getOffer() {
        return offer;
    }

    public void setOffer(BigDecimal offer) {
        this.offer = offer;
    }

    public Integer getEntrySize() {
        return entrySize;
    }

    public void setEntrySize(Integer entrySize) {
        this.entrySize = entrySize;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Date sendingTime) {
        this.sendingTime = sendingTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marketDataId != null ? marketDataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarketData)) {
            return false;
        }
        MarketData other = (MarketData) object;
        if ((this.marketDataId == null && other.marketDataId != null) || (this.marketDataId != null && !this.marketDataId.equals(other.marketDataId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.MarketData[ marketDataId=" + marketDataId + " ]";
    }
    
}
