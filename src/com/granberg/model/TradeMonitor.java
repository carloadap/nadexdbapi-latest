/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "trade_monitor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TradeMonitor.findAll", query = "SELECT t FROM TradeMonitor t"),
    @NamedQuery(name = "TradeMonitor.findByTradeMonitorId", query = "SELECT t FROM TradeMonitor t WHERE t.tradeMonitorId = :tradeMonitorId"),
    @NamedQuery(name = "TradeMonitor.findByInstrumentName", query = "SELECT t FROM TradeMonitor t WHERE t.instrumentName = :instrumentName"),
    @NamedQuery(name = "TradeMonitor.findByEntryOrderTypeId", query = "SELECT t FROM TradeMonitor t WHERE t.entryOrderTypeId = :entryOrderTypeId"),
    @NamedQuery(name = "TradeMonitor.findByEntryPrice", query = "SELECT t FROM TradeMonitor t WHERE t.entryPrice = :entryPrice"),
    @NamedQuery(name = "TradeMonitor.findByProfitTargetPrice", query = "SELECT t FROM TradeMonitor t WHERE t.profitTargetPrice = :profitTargetPrice"),
    @NamedQuery(name = "TradeMonitor.findByStopLossPrice", query = "SELECT t FROM TradeMonitor t WHERE t.stopLossPrice = :stopLossPrice"),
    @NamedQuery(name = "TradeMonitor.findByMaxPriceOnEntry", query = "SELECT t FROM TradeMonitor t WHERE t.maxPriceOnEntry = :maxPriceOnEntry"),
    @NamedQuery(name = "TradeMonitor.findByMinPriceOnEntry", query = "SELECT t FROM TradeMonitor t WHERE t.minPriceOnEntry = :minPriceOnEntry"),
    @NamedQuery(name = "TradeMonitor.findByCurrentPrice", query = "SELECT t FROM TradeMonitor t WHERE t.currentPrice = :currentPrice"),
    @NamedQuery(name = "TradeMonitor.findByExitPrice", query = "SELECT t FROM TradeMonitor t WHERE t.exitPrice = :exitPrice"),
    @NamedQuery(name = "TradeMonitor.findByEntryDate", query = "SELECT t FROM TradeMonitor t WHERE t.entryDate > :entryDate"),
    @NamedQuery(name = "TradeMonitor.findByExitDate", query = "SELECT t FROM TradeMonitor t WHERE t.exitDate = :exitDate"),
    @NamedQuery(name = "TradeMonitor.findByUpdatedOn", query = "SELECT t FROM TradeMonitor t WHERE t.updatedOn = :updatedOn")})
public class TradeMonitor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trade_monitor_id")
    private Integer tradeMonitorId;
    @Column(name = "instrument_name")
    private String instrumentName;
    @Basic(optional = false)
    @Column(name = "entry_order_type_id")
    private int entryOrderTypeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "entry_price")
    private BigDecimal entryPrice;
    @Column(name = "profit_target_price")
    private BigDecimal profitTargetPrice;
    @Column(name = "stop_loss_price")
    private BigDecimal stopLossPrice;
    @Column(name = "max_price_on_entry")
    private BigDecimal maxPriceOnEntry;
    @Column(name = "min_price_on_entry")
    private BigDecimal minPriceOnEntry;
    @Column(name = "current_price")
    private BigDecimal currentPrice;
    @Column(name = "exit_price")
    private BigDecimal exitPrice;
    @Column(name = "entry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;
    @Column(name = "exit_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date exitDate;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TradeMonitor() {
    }

    public TradeMonitor(Integer tradeMonitorId) {
        this.tradeMonitorId = tradeMonitorId;
    }

    public TradeMonitor(Integer tradeMonitorId, int entryOrderTypeId) {
        this.tradeMonitorId = tradeMonitorId;
        this.entryOrderTypeId = entryOrderTypeId;
    }

    public Integer getTradeMonitorId() {
        return tradeMonitorId;
    }

    public void setTradeMonitorId(Integer tradeMonitorId) {
        this.tradeMonitorId = tradeMonitorId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public int getEntryOrderTypeId() {
        return entryOrderTypeId;
    }

    public void setEntryOrderTypeId(int entryOrderTypeId) {
        this.entryOrderTypeId = entryOrderTypeId;
    }

    public BigDecimal getEntryPrice() {
        return entryPrice;
    }

    public void setEntryPrice(BigDecimal entryPrice) {
        this.entryPrice = entryPrice;
    }

    public BigDecimal getProfitTargetPrice() {
        return profitTargetPrice;
    }

    public void setProfitTargetPrice(BigDecimal profitTargetPrice) {
        this.profitTargetPrice = profitTargetPrice;
    }

    public BigDecimal getStopLossPrice() {
        return stopLossPrice;
    }

    public void setStopLossPrice(BigDecimal stopLossPrice) {
        this.stopLossPrice = stopLossPrice;
    }

    public BigDecimal getMaxPriceOnEntry() {
        return maxPriceOnEntry;
    }

    public void setMaxPriceOnEntry(BigDecimal maxPriceOnEntry) {
        this.maxPriceOnEntry = maxPriceOnEntry;
    }

    public BigDecimal getMinPriceOnEntry() {
        return minPriceOnEntry;
    }

    public void setMinPriceOnEntry(BigDecimal minPriceOnEntry) {
        this.minPriceOnEntry = minPriceOnEntry;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getExitPrice() {
        return exitPrice;
    }

    public void setExitPrice(BigDecimal exitPrice) {
        this.exitPrice = exitPrice;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getExitDate() {
        return exitDate;
    }

    public void setExitDate(Date exitDate) {
        this.exitDate = exitDate;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tradeMonitorId != null ? tradeMonitorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TradeMonitor)) {
            return false;
        }
        TradeMonitor other = (TradeMonitor) object;
        if ((this.tradeMonitorId == null && other.tradeMonitorId != null) || (this.tradeMonitorId != null && !this.tradeMonitorId.equals(other.tradeMonitorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.TradeMonitor[ tradeMonitorId=" + tradeMonitorId + " ]";
    }
    
}
