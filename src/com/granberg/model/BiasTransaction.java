/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "bias_transaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BiasTransaction.findAll", query = "SELECT b FROM BiasTransaction b"),
    @NamedQuery(name = "BiasTransaction.findByBiasTransactionId", query = "SELECT b FROM BiasTransaction b WHERE b.biasTransactionId = :biasTransactionId"),
    @NamedQuery(name = "BiasTransaction.findByBiasId", query = "SELECT b FROM BiasTransaction b WHERE b.biasId = :biasId"),
    @NamedQuery(name = "BiasTransaction.findByInstrumentName", query = "SELECT b FROM BiasTransaction b WHERE b.instrumentName = :instrumentName"),
    @NamedQuery(name = "BiasTransaction.findByPrice", query = "SELECT b FROM BiasTransaction b WHERE b.price = :price"),
    @NamedQuery(name = "BiasTransaction.findByHighReach", query = "SELECT b FROM BiasTransaction b WHERE b.highReach = :highReach"),
    @NamedQuery(name = "BiasTransaction.findByLowReach", query = "SELECT b FROM BiasTransaction b WHERE b.lowReach = :lowReach"),
    @NamedQuery(name = "BiasTransaction.findByCurrentPrice", query = "SELECT b FROM BiasTransaction b WHERE b.currentPrice = :currentPrice"),
    @NamedQuery(name = "BiasTransaction.findByPipsMove", query = "SELECT b FROM BiasTransaction b WHERE b.pipsMove = :pipsMove"),
    @NamedQuery(name = "BiasTransaction.findByLastTime", query = "SELECT b FROM BiasTransaction b WHERE b.lastTime = :lastTime")})
public class BiasTransaction implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bias_transaction_id")
    private Integer biasTransactionId;
    @Column(name = "bias_id")
    private Integer biasId;
    @Column(name = "instrument_name")
    private String instrumentName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "high_reach")
    private BigDecimal highReach;
    @Column(name = "low_reach")
    private BigDecimal lowReach;
    @Column(name = "current_price")
    private BigDecimal currentPrice;
    @Column(name = "pips_move")
    private BigDecimal pipsMove;
    @Column(name = "last_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTime;

    public BiasTransaction() {
    }

    public BiasTransaction(Integer biasTransactionId) {
        this.biasTransactionId = biasTransactionId;
    }

    public Integer getBiasTransactionId() {
        return biasTransactionId;
    }

    public void setBiasTransactionId(Integer biasTransactionId) {
        this.biasTransactionId = biasTransactionId;
    }

    public Integer getBiasId() {
        return biasId;
    }

    public void setBiasId(Integer biasId) {
        this.biasId = biasId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getHighReach() {
        return highReach;
    }

    public void setHighReach(BigDecimal highReach) {
        this.highReach = highReach;
    }

    public BigDecimal getLowReach() {
        return lowReach;
    }

    public void setLowReach(BigDecimal lowReach) {
        this.lowReach = lowReach;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getPipsMove() {
        return pipsMove;
    }

    public void setPipsMove(BigDecimal pipsMove) {
        this.pipsMove = pipsMove;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (biasTransactionId != null ? biasTransactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BiasTransaction)) {
            return false;
        }
        BiasTransaction other = (BiasTransaction) object;
        if ((this.biasTransactionId == null && other.biasTransactionId != null) || (this.biasTransactionId != null && !this.biasTransactionId.equals(other.biasTransactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.BiasTransaction[ biasTransactionId=" + biasTransactionId + " ]";
    }
    
}
