/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Carlo
 */
public class BollingerBand {
    
    
    private double SMA;
    private double EMA;
    private double HBAND;
    private double LBAND;

    /**
     * @return the SMA
     */
    public double getSMA() {
        return SMA;
    }

    /**
     * @param SMA the SMA to set
     */
    public void setSMA(double SMA) {
        this.SMA = SMA;
    }

    /**
     * @return the EMA
     */
    public double getEMA() {
        return EMA;
    }

    /**
     * @param EMA the EMA to set
     */
    public void setEMA(double EMA) {
        this.EMA = EMA;
    }

    /**
     * @return the HBAND
     */
    public double getHBAND() {
        return HBAND;
    }

    /**
     * @param HBAND the HBAND to set
     */
    public void setHBAND(double HBAND) {
        this.HBAND = HBAND;
    }

    /**
     * @return the LBAND
     */
    public double getLBAND() {
        return LBAND;
    }

    /**
     * @param LBAND the LBAND to set
     */
    public void setLBAND(double LBAND) {
        this.LBAND = LBAND;
    }
}
