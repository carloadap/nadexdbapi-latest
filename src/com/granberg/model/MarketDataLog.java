/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "MarketDataLog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MarketDataLog.findAll", query = "SELECT m FROM MarketDataLog m"),
    @NamedQuery(name = "MarketDataLog.findByMarketDataLogId", query = "SELECT m FROM MarketDataLog m WHERE m.marketDataLogId = :marketDataLogId"),
    @NamedQuery(name = "MarketDataLog.findBySymbol", query = "SELECT m FROM MarketDataLog m WHERE m.symbol = :symbol"),
    @NamedQuery(name = "MarketDataLog.findByLatestSymbol", query = "SELECT MAX(m.timestamp) FROM MarketDataLog m WHERE m.symbol = :symbol AND m.priceUpdated = :priceUpdated"),
    @NamedQuery(name = "MarketDataLog.findByBid", query = "SELECT m FROM MarketDataLog m WHERE m.bid = :bid"),
    @NamedQuery(name = "MarketDataLog.findByOffer", query = "SELECT m FROM MarketDataLog m WHERE m.offer = :offer"),
    @NamedQuery(name = "MarketDataLog.findByStrikePrice", query = "SELECT m FROM MarketDataLog m WHERE m.strikePrice = :strikePrice"),
    @NamedQuery(name = "MarketDataLog.findByPriceUpdated", query = "SELECT m FROM MarketDataLog m WHERE m.priceUpdated = :priceUpdated"),
    @NamedQuery(name = "MarketDataLog.findByTimestamp", query = "SELECT m FROM MarketDataLog m WHERE m.timestamp = :timestamp")})
public class MarketDataLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "market_data_log_id")
    private Integer marketDataLogId;
    @Column(name = "symbol")
    private String symbol;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "bid")
    private BigDecimal bid;
    @Column(name = "offer")
    private BigDecimal offer;
    @Column(name = "strike_price")
    private BigDecimal strikePrice;
    @Column(name = "price_updated")
    private Integer priceUpdated;
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public MarketDataLog() {
    }

    public MarketDataLog(Integer marketDataLogId) {
        this.marketDataLogId = marketDataLogId;
    }

    public Integer getMarketDataLogId() {
        return marketDataLogId;
    }

    public void setMarketDataLogId(Integer marketDataLogId) {
        this.marketDataLogId = marketDataLogId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public BigDecimal getOffer() {
        return offer;
    }

    public void setOffer(BigDecimal offer) {
        this.offer = offer;
    }

    public BigDecimal getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(BigDecimal strikePrice) {
        this.strikePrice = strikePrice;
    }

    public Integer getPriceUpdated() {
        return priceUpdated;
    }

    public void setPriceUpdated(Integer priceUpdated) {
        this.priceUpdated = priceUpdated;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marketDataLogId != null ? marketDataLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarketDataLog)) {
            return false;
        }
        MarketDataLog other = (MarketDataLog) object;
        if ((this.marketDataLogId == null && other.marketDataLogId != null) || (this.marketDataLogId != null && !this.marketDataLogId.equals(other.marketDataLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.MarketDataLog[ marketDataLogId=" + marketDataLogId + " ]";
    }
    
}
