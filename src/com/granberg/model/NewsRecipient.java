/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "news_recipient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsRecipient.findAll", query = "SELECT n FROM NewsRecipient n"),
    @NamedQuery(name = "NewsRecipient.findByRecipientId", query = "SELECT n FROM NewsRecipient n WHERE n.recipientId = :recipientId"),
    @NamedQuery(name = "NewsRecipient.findByFirstName", query = "SELECT n FROM NewsRecipient n WHERE n.firstName = :firstName"),
    @NamedQuery(name = "NewsRecipient.findByLastName", query = "SELECT n FROM NewsRecipient n WHERE n.lastName = :lastName"),
    @NamedQuery(name = "NewsRecipient.findByEmailAddress", query = "SELECT n FROM NewsRecipient n WHERE n.emailAddress = :emailAddress")})
public class NewsRecipient implements Serializable {
    @Column(name = "active")
    private Boolean active;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "recipient_id")
    private Integer recipientId;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email_address")
    private String emailAddress;

    public NewsRecipient() {
    }

    public NewsRecipient(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recipientId != null ? recipientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsRecipient)) {
            return false;
        }
        NewsRecipient other = (NewsRecipient) object;
        if ((this.recipientId == null && other.recipientId != null) || (this.recipientId != null && !this.recipientId.equals(other.recipientId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.NewsRecipient[ recipientId=" + recipientId + " ]";
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
}
