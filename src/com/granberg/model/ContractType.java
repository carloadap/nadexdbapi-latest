/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "ContractType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractType.findAll", query = "SELECT c FROM ContractType c"),
    @NamedQuery(name = "ContractType.findByContractTypeId", query = "SELECT c FROM ContractType c WHERE c.contractTypeId = :contractTypeId"),
    @NamedQuery(name = "ContractType.findByNadexCode", query = "SELECT c FROM ContractType c WHERE c.nadexCode = :nadexCode"),
    @NamedQuery(name = "ContractType.findByDescription", query = "SELECT c FROM ContractType c WHERE c.description = :description")})
public class ContractType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contract_type_id")
    private Integer contractTypeId;
    @Column(name = "nadex_code")
    private String nadexCode;
    @Column(name = "description")
    private String description;

    public ContractType() {
    }

    public ContractType(Integer contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public Integer getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Integer contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getNadexCode() {
        return nadexCode;
    }

    public void setNadexCode(String nadexCode) {
        this.nadexCode = nadexCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractTypeId != null ? contractTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractType)) {
            return false;
        }
        ContractType other = (ContractType) object;
        if ((this.contractTypeId == null && other.contractTypeId != null) || (this.contractTypeId != null && !this.contractTypeId.equals(other.contractTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.ContractType[ contractTypeId=" + contractTypeId + " ]";
    }
    
}
