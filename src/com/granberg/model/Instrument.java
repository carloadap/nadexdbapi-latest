/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Cache;

/**
 *
 * @author Carlo
 */
@Entity
//@Cache(alwaysRefresh = true, expiry = -1)
@Table(name = "Instrument")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Instrument.findAll", query = "SELECT i FROM Instrument i"),
    @NamedQuery(name = "Instrument.findByInstrumentId", query = "SELECT i FROM Instrument i WHERE i.instrumentId = :instrumentId"),
    @NamedQuery(name = "Instrument.findByInstrumentIdByCurrency", query = "SELECT i FROM Instrument i WHERE i.instrumentId = :instrumentId AND i.instrumentTypeId = :instrumentTypeId"),
    @NamedQuery(name = "Instrument.findByInstrumentName", query = "SELECT i FROM Instrument i WHERE i.instrumentName = :instrumentName"),
    @NamedQuery(name = "Instrument.findByInstrumentTypeId", query = "SELECT i FROM Instrument i WHERE i.instrumentTypeId = :instrumentTypeId"),
    @NamedQuery(name = "Instrument.findByInstrumentPrice", query = "SELECT i FROM Instrument i WHERE i.instrumentPrice = :instrumentPrice"),
    @NamedQuery(name = "Instrument.findByDateLatestInstrumentPrice", query = "SELECT DISTINCT MAX(i.updatedOn) FROM Instrument i"),
    @NamedQuery(name = "Instrument.findByUpdatedOn", query = "SELECT i FROM Instrument i WHERE i.updatedOn = :updatedOn")})
public class Instrument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "instrument_id")
    private Integer instrumentId;
    @Basic(optional = false)
    @Column(name = "instrument_name")
    private String instrumentName;
    @Basic(optional = false)
    @Column(name = "instrument_type_id")
    private int instrumentTypeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "instrument_price")
    private BigDecimal instrumentPrice;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public Instrument() {
    }

    public Instrument(Integer instrumentId) {
        this.instrumentId = instrumentId;
    }

    public Instrument(Integer instrumentId, String instrumentName, int instrumentTypeId) {
        this.instrumentId = instrumentId;
        this.instrumentName = instrumentName;
        this.instrumentTypeId = instrumentTypeId;
    }

    public Integer getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(Integer instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public int getInstrumentTypeId() {
        return instrumentTypeId;
    }

    public void setInstrumentTypeId(int instrumentTypeId) {
        this.instrumentTypeId = instrumentTypeId;
    }

    public BigDecimal getInstrumentPrice() {
        return instrumentPrice;
    }

    public void setInstrumentPrice(BigDecimal instrumentPrice) {
        this.instrumentPrice = instrumentPrice;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrumentId != null ? instrumentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instrument)) {
            return false;
        }
        Instrument other = (Instrument) object;
        if ((this.instrumentId == null && other.instrumentId != null) || (this.instrumentId != null && !this.instrumentId.equals(other.instrumentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Instrument[ instrumentId=" + instrumentId + " ]";
    }
    
}
