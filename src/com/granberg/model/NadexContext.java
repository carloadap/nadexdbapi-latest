/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "NadexContext")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NadexContext.findAll", query = "SELECT n FROM NadexContext n"),
    @NamedQuery(name = "NadexContext.findByNadexContextId", query = "SELECT n FROM NadexContext n WHERE n.nadexContextId = :nadexContextId"),
    @NamedQuery(name = "NadexContext.findByTradingSessionStatus", query = "SELECT n FROM NadexContext n WHERE n.tradingSessionStatus = :tradingSessionStatus"),
    @NamedQuery(name = "NadexContext.findByIsLoggedIn", query = "SELECT n FROM NadexContext n WHERE n.isLoggedIn = :isLoggedIn"),
    @NamedQuery(name = "NadexContext.findByLoggedInDate", query = "SELECT n FROM NadexContext n WHERE n.loggedInDate = :loggedInDate")})
public class NadexContext implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "nadex_context_id")
    private Integer nadexContextId;
    @Column(name = "trading_session_status")
    private String tradingSessionStatus;
    @Column(name = "is_logged_in")
    private Boolean isLoggedIn;
    @Column(name = "logged_in_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loggedInDate;

    public NadexContext() {
    }

    public NadexContext(Integer nadexContextId) {
        this.nadexContextId = nadexContextId;
    }

    public Integer getNadexContextId() {
        return nadexContextId;
    }

    public void setNadexContextId(Integer nadexContextId) {
        this.nadexContextId = nadexContextId;
    }

    public String getTradingSessionStatus() {
        return tradingSessionStatus;
    }

    public void setTradingSessionStatus(String tradingSessionStatus) {
        this.tradingSessionStatus = tradingSessionStatus;
    }

    public Boolean getIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(Boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public Date getLoggedInDate() {
        return loggedInDate;
    }

    public void setLoggedInDate(Date loggedInDate) {
        this.loggedInDate = loggedInDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nadexContextId != null ? nadexContextId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NadexContext)) {
            return false;
        }
        NadexContext other = (NadexContext) object;
        if ((this.nadexContextId == null && other.nadexContextId != null) || (this.nadexContextId != null && !this.nadexContextId.equals(other.nadexContextId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.NadexContext[ nadexContextId=" + nadexContextId + " ]";
    }
    
}
