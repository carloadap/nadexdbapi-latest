/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "ADR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adr.findAll", query = "SELECT a FROM Adr a"),
    @NamedQuery(name = "Adr.findByAdrId", query = "SELECT a FROM Adr a WHERE a.adrId = :adrId"),
    @NamedQuery(name = "Adr.findByInstrumentName", query = "SELECT a FROM Adr a WHERE a.instrumentName = :instrumentName"),
    @NamedQuery(name = "Adr.findByInstrumentNameAndDate", query = "SELECT a FROM Adr a WHERE a.instrumentName = :instrumentName AND a.date = :date"),
    @NamedQuery(name = "Adr.findByAdr", query = "SELECT a FROM Adr a WHERE a.adr = :adr"),
    @NamedQuery(name = "Adr.findByHighLimit", query = "SELECT a FROM Adr a WHERE a.highLimit = :highLimit"),
    @NamedQuery(name = "Adr.findByLowLimit", query = "SELECT a FROM Adr a WHERE a.lowLimit = :lowLimit"),
    @NamedQuery(name = "Adr.findByCurrentHigh", query = "SELECT a FROM Adr a WHERE a.currentHigh = :currentHigh"),
    @NamedQuery(name = "Adr.findByCurrentLow", query = "SELECT a FROM Adr a WHERE a.currentLow = :currentLow"),
    @NamedQuery(name = "Adr.findByExhaustion", query = "SELECT a FROM Adr a WHERE a.exhaustion = :exhaustion"),
    @NamedQuery(name = "Adr.findBySessionId", query = "SELECT a FROM Adr a WHERE a.sessionId = :sessionId"),
    @NamedQuery(name = "Adr.findByDateAndSessionId", query = "SELECT a FROM Adr a WHERE a.sessionId = :sessionId AND a.date = :date"),
    @NamedQuery(name = "Adr.findByInstrumentNameAndDateAndSessionId", query = "SELECT a FROM Adr a WHERE a.instrumentName = :instrumentName AND a.sessionId = :sessionId AND a.date = :date"),
    @NamedQuery(name = "Adr.findBySessionLatestADR", query = "SELECT DISTINCT MAX(a.date) FROM Adr a WHERE a.sessionId = :sessionId"),
    @NamedQuery(name = "Adr.findByLatestADR", query = "SELECT DISTINCT MAX(a.date) FROM Adr a"),
    @NamedQuery(name = "Adr.findByDate", query = "SELECT a FROM Adr a WHERE a.date = :date")})
public class Adr implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "adr_id")
    private Integer adrId;
    @Column(name = "instrument_name")
    private String instrumentName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "adr")
    private BigDecimal adr;
    @Column(name = "high_limit")
    private BigDecimal highLimit;
    @Column(name = "low_limit")
    private BigDecimal lowLimit;
    @Column(name = "current_high")
    private BigDecimal currentHigh;
    @Column(name = "current_low")
    private BigDecimal currentLow;
    @Column(name = "exhaustion")
    private BigDecimal exhaustion;
    @Column(name = "session_id")
    private Integer sessionId;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Adr() {
    }

    public Adr(Integer adrId) {
        this.adrId = adrId;
    }

    public Integer getAdrId() {
        return adrId;
    }

    public void setAdrId(Integer adrId) {
        this.adrId = adrId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public BigDecimal getAdr() {
        return adr;
    }

    public void setAdr(BigDecimal adr) {
        this.adr = adr;
    }

    public BigDecimal getHighLimit() {
        return highLimit;
    }

    public void setHighLimit(BigDecimal highLimit) {
        this.highLimit = highLimit;
    }

    public BigDecimal getLowLimit() {
        return lowLimit;
    }

    public void setLowLimit(BigDecimal lowLimit) {
        this.lowLimit = lowLimit;
    }

    public BigDecimal getCurrentHigh() {
        return currentHigh;
    }

    public void setCurrentHigh(BigDecimal currentHigh) {
        this.currentHigh = currentHigh;
    }

    public BigDecimal getCurrentLow() {
        return currentLow;
    }

    public void setCurrentLow(BigDecimal currentLow) {
        this.currentLow = currentLow;
    }

    public BigDecimal getExhaustion() {
        return exhaustion;
    }

    public void setExhaustion(BigDecimal exhaustion) {
        this.exhaustion = exhaustion;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adrId != null ? adrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adr)) {
            return false;
        }
        Adr other = (Adr) object;
        if ((this.adrId == null && other.adrId != null) || (this.adrId != null && !this.adrId.equals(other.adrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Adr[ adrId=" + adrId + " ]";
    }
    
}
