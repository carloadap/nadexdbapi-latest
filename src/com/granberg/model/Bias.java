/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "bias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bias.findAll", query = "SELECT b FROM Bias b"),
    @NamedQuery(name = "Bias.findByBiasId", query = "SELECT b FROM Bias b WHERE b.biasId = :biasId"),
    @NamedQuery(name = "Bias.findByName", query = "SELECT b FROM Bias b WHERE b.name = :name"),
    @NamedQuery(name = "Bias.findByEmail", query = "SELECT b FROM Bias b WHERE b.email = :email")})
public class Bias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bias_id")
    private Integer biasId;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;

    public Bias() {
    }

    public Bias(Integer biasId) {
        this.biasId = biasId;
    }

    public Integer getBiasId() {
        return biasId;
    }

    public void setBiasId(Integer biasId) {
        this.biasId = biasId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (biasId != null ? biasId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bias)) {
            return false;
        }
        Bias other = (Bias) object;
        if ((this.biasId == null && other.biasId != null) || (this.biasId != null && !this.biasId.equals(other.biasId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Bias[ biasId=" + biasId + " ]";
    }
    
}
