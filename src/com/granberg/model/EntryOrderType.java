/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "entry_order_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntryOrderType.findAll", query = "SELECT e FROM EntryOrderType e"),
    @NamedQuery(name = "EntryOrderType.findByEntryOrderTypeId", query = "SELECT e FROM EntryOrderType e WHERE e.entryOrderTypeId = :entryOrderTypeId"),
    @NamedQuery(name = "EntryOrderType.findByEntryTypeId", query = "SELECT e FROM EntryOrderType e WHERE e.entryTypeId = :entryTypeId"),
    @NamedQuery(name = "EntryOrderType.findByEntryOrderTypeDescription", query = "SELECT e FROM EntryOrderType e WHERE e.entryOrderTypeDescription = :entryOrderTypeDescription")})
public class EntryOrderType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "entry_order_type_id")
    private Integer entryOrderTypeId;
    @Column(name = "entry_type_id")
    private Integer entryTypeId;
    @Column(name = "entry_order_type_description")
    private String entryOrderTypeDescription;

    public EntryOrderType() {
    }

    public EntryOrderType(Integer entryOrderTypeId) {
        this.entryOrderTypeId = entryOrderTypeId;
    }

    public Integer getEntryOrderTypeId() {
        return entryOrderTypeId;
    }

    public void setEntryOrderTypeId(Integer entryOrderTypeId) {
        this.entryOrderTypeId = entryOrderTypeId;
    }

    public Integer getEntryTypeId() {
        return entryTypeId;
    }

    public void setEntryTypeId(Integer entryTypeId) {
        this.entryTypeId = entryTypeId;
    }

    public String getEntryOrderTypeDescription() {
        return entryOrderTypeDescription;
    }

    public void setEntryOrderTypeDescription(String entryOrderTypeDescription) {
        this.entryOrderTypeDescription = entryOrderTypeDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (entryOrderTypeId != null ? entryOrderTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntryOrderType)) {
            return false;
        }
        EntryOrderType other = (EntryOrderType) object;
        if ((this.entryOrderTypeId == null && other.entryOrderTypeId != null) || (this.entryOrderTypeId != null && !this.entryOrderTypeId.equals(other.entryOrderTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.EntryOrderType[ entryOrderTypeId=" + entryOrderTypeId + " ]";
    }
    
}
