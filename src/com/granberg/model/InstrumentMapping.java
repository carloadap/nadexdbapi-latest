/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "InstrumentMapping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstrumentMapping.findAll", query = "SELECT i FROM InstrumentMapping i"),
    @NamedQuery(name = "InstrumentMapping.findByInstrumentMappingId", query = "SELECT i FROM InstrumentMapping i WHERE i.instrumentMappingId = :instrumentMappingId"),
    @NamedQuery(name = "InstrumentMapping.findByInstrumentName", query = "SELECT i FROM InstrumentMapping i WHERE i.instrumentName = :instrumentName"),
    @NamedQuery(name = "InstrumentMapping.findByAlternateName", query = "SELECT i FROM InstrumentMapping i WHERE i.alternateName = :alternateName")})
public class InstrumentMapping implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "instrument_mapping_id")
    private Integer instrumentMappingId;
    @Column(name = "instrument_name")
    private String instrumentName;
    @Column(name = "alternate_name")
    private String alternateName;

    public InstrumentMapping() {
    }

    public InstrumentMapping(Integer instrumentMappingId) {
        this.instrumentMappingId = instrumentMappingId;
    }

    public Integer getInstrumentMappingId() {
        return instrumentMappingId;
    }

    public void setInstrumentMappingId(Integer instrumentMappingId) {
        this.instrumentMappingId = instrumentMappingId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrumentMappingId != null ? instrumentMappingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrumentMapping)) {
            return false;
        }
        InstrumentMapping other = (InstrumentMapping) object;
        if ((this.instrumentMappingId == null && other.instrumentMappingId != null) || (this.instrumentMappingId != null && !this.instrumentMappingId.equals(other.instrumentMappingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.InstrumentMapping[ instrumentMappingId=" + instrumentMappingId + " ]";
    }
    
}
