/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Contract")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contract.findAll", query = "SELECT c FROM Contract c"),
    @NamedQuery(name = "Contract.findByContractId", query = "SELECT c FROM Contract c WHERE c.contractId = :contractId"),
    @NamedQuery(name = "Contract.findByContractIdNotExpired", query = "SELECT c FROM Contract c WHERE c.contractId = :contractId AND c.expirationDate > :expirationDate"),
    @NamedQuery(name = "Contract.findByContractSymbolNotExpired", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol AND c.expirationDate > :expirationDate"),
    @NamedQuery(name = "Contract.findBySecurityId", query = "SELECT c FROM Contract c WHERE c.securityId = :securityId"),
    @NamedQuery(name = "Contract.findByContractSymbol", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol"),
    @NamedQuery(name = "Contract.findByContractSymbolOrderByDate", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol ORDER BY c.expirationDate DESC"),
    @NamedQuery(name = "Contract.findByContractSymbolAndExpiration", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol AND c.expirationDate >= :expirationDate"),
    @NamedQuery(name = "Contract.findByContractSymbolSecurityIdExpirationDate", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol AND c.securityId = :securityId AND c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByContractSymbolExpirationDateTest", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol AND c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByContractIdSecurityIdExpirationDate", query = "SELECT c FROM Contract c WHERE c.contractId = :contractId AND c.securityId = :securityId AND c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByContractInstrumentId", query = "SELECT c FROM Contract c WHERE c.instrumentId = :instrumentId AND c.expirationDate > :expirationDate"),
    @NamedQuery(name = "Contract.findByContractDescription", query = "SELECT c FROM Contract c WHERE c.contractDescription = :contractDescription"),
    @NamedQuery(name = "Contract.findByInstrumentId", query = "SELECT c FROM Contract c WHERE c.instrumentId = :instrumentId"),
    @NamedQuery(name = "Contract.findByOrderTypeId", query = "SELECT c FROM Contract c WHERE c.orderTypeId = :orderTypeId"),
    @NamedQuery(name = "Contract.findByCeiling", query = "SELECT c FROM Contract c WHERE c.ceiling = :ceiling"),
    @NamedQuery(name = "Contract.findContractListBySymbolAndExpirationDate", query = "SELECT c FROM Contract c WHERE c.contractSymbol = :contractSymbol AND c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByFloor", query = "SELECT c FROM Contract c WHERE c.floor = :floor"),
    @NamedQuery(name = "Contract.findByExpirationDate", query = "SELECT c FROM Contract c WHERE c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findNotExpired", query = "SELECT c FROM Contract c WHERE c.expirationDate > :expirationDate"),
    @NamedQuery(name = "Contract.findByActive", query = "SELECT c FROM Contract c WHERE c.active = :active"),
    @NamedQuery(name = "Contract.findByContractTypeId", query = "SELECT c FROM Contract c WHERE c.contractTypeId = :contractTypeId"),
    @NamedQuery(name = "Contract.findActiveContractByContractTypeIdAndInstrumentId", query = "SELECT c FROM Contract c WHERE c.contractTypeId = :contractTypeId AND c.instrumentId = :instrumentId AND c.expirationDate < :currentDate"),
    @NamedQuery(name = "Contract.findActiveContractByContractTypeIdAndInstrumentIdTest", query = "SELECT c FROM Contract c WHERE c.contractTypeId = :contractTypeId AND c.instrumentId = :instrumentId"),
    @NamedQuery(name = "Contract.findByUpdatedOn", query = "SELECT c FROM Contract c WHERE c.updatedOn = :updatedOn"),
    @NamedQuery(name = "Contract.findByPeriodicityId", query = "SELECT c FROM Contract c WHERE c.periodicityId = :periodicityId"),
    @NamedQuery(name = "Contract.findByStrikePrice", query = "SELECT c FROM Contract c WHERE c.strikePrice = :strikePrice"),
    @NamedQuery(name = "Contract.findByStrikePriceAndPeriod", query = "SELECT c FROM Contract c WHERE c.strikePrice = :strikePrice AND c.periodicityId = :periodicityId"),
    @NamedQuery(name = "Contract.findByStrikePriceAndExpirationDate", query = "SELECT c FROM Contract c WHERE c.strikePrice = :strikePrice and c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByOpeningDate", query = "SELECT c FROM Contract c WHERE c.openingDate = :openingDate")})
public class Contract implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contract_id")
    private Integer contractId;
    @Column(name = "security_id")
    private String securityId;
    @Column(name = "contract_symbol")
    private String contractSymbol;
    @Column(name = "contract_description")
    private String contractDescription;
    @Column(name = "instrument_id")
    private Integer instrumentId;
    @Column(name = "order_type_id")
    private Integer orderTypeId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ceiling")
    private BigDecimal ceiling;
    @Column(name = "floor")
    private BigDecimal floor;
    @Column(name = "expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Column(name = "active")
    private String active;
    @Column(name = "contract_type_id")
    private Integer contractTypeId;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @Column(name = "periodicity_id")
    private Integer periodicityId;
    @Column(name = "strike_price")
    private BigDecimal strikePrice;
    @Column(name = "opening_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openingDate;

    public Contract() {
    }

    public Contract(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getSecurityId() {
        return securityId;
    }

    public void setSecurityId(String securityId) {
        this.securityId = securityId;
    }

    public String getContractSymbol() {
        return contractSymbol;
    }

    public void setContractSymbol(String contractSymbol) {
        this.contractSymbol = contractSymbol;
    }

    public String getContractDescription() {
        return contractDescription;
    }

    public void setContractDescription(String contractDescription) {
        this.contractDescription = contractDescription;
    }

    public Integer getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(Integer instrumentId) {
        this.instrumentId = instrumentId;
    }

    public Integer getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(Integer orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public BigDecimal getCeiling() {
        return ceiling;
    }

    public void setCeiling(BigDecimal ceiling) {
        this.ceiling = ceiling;
    }

    public BigDecimal getFloor() {
        return floor;
    }

    public void setFloor(BigDecimal floor) {
        this.floor = floor;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Integer contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getPeriodicityId() {
        return periodicityId;
    }

    public void setPeriodicityId(Integer periodicityId) {
        this.periodicityId = periodicityId;
    }

    public BigDecimal getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(BigDecimal strikePrice) {
        this.strikePrice = strikePrice;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractId != null ? contractId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contract)) {
            return false;
        }
        Contract other = (Contract) object;
        if ((this.contractId == null && other.contractId != null) || (this.contractId != null && !this.contractId.equals(other.contractId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Contract[ contractId=" + contractId + " ]";
    }
    
}
