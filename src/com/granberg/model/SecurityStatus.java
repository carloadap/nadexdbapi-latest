/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "SecurityStatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SecurityStatus.findAll", query = "SELECT s FROM SecurityStatus s"),
    @NamedQuery(name = "SecurityStatus.findBySecurityStatusId", query = "SELECT s FROM SecurityStatus s WHERE s.securityStatusId = :securityStatusId"),
    @NamedQuery(name = "SecurityStatus.findBySymbol", query = "SELECT s FROM SecurityStatus s WHERE s.symbol = :symbol"),
    @NamedQuery(name = "SecurityStatus.findByLastpx", query = "SELECT s FROM SecurityStatus s WHERE s.lastpx = :lastpx"),
    @NamedQuery(name = "SecurityStatus.findBySecurityStatusRequestId", query = "SELECT s FROM SecurityStatus s WHERE s.securityStatusRequestId = :securityStatusRequestId"),
    @NamedQuery(name = "SecurityStatus.findBySendingTime", query = "SELECT s FROM SecurityStatus s WHERE s.sendingTime = :sendingTime")})
public class SecurityStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "security_status_id")
    private Integer securityStatusId;
    @Column(name = "symbol")
    private String symbol;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "lastpx")
    private BigDecimal lastpx;
    @Column(name = "security_status_request_id")
    private Integer securityStatusRequestId;
    @Column(name = "sending_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendingTime;

    public SecurityStatus() {
    }

    public SecurityStatus(Integer securityStatusId) {
        this.securityStatusId = securityStatusId;
    }

    public Integer getSecurityStatusId() {
        return securityStatusId;
    }

    public void setSecurityStatusId(Integer securityStatusId) {
        this.securityStatusId = securityStatusId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getLastpx() {
        return lastpx;
    }

    public void setLastpx(BigDecimal lastpx) {
        this.lastpx = lastpx;
    }

    public Integer getSecurityStatusRequestId() {
        return securityStatusRequestId;
    }

    public void setSecurityStatusRequestId(Integer securityStatusRequestId) {
        this.securityStatusRequestId = securityStatusRequestId;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Date sendingTime) {
        this.sendingTime = sendingTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (securityStatusId != null ? securityStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityStatus)) {
            return false;
        }
        SecurityStatus other = (SecurityStatus) object;
        if ((this.securityStatusId == null && other.securityStatusId != null) || (this.securityStatusId != null && !this.securityStatusId.equals(other.securityStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.SecurityStatus[ securityStatusId=" + securityStatusId + " ]";
    }
    
}
