/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Trend")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trend.findAll", query = "SELECT t FROM Trend t"),
    @NamedQuery(name = "Trend.findByTrendId", query = "SELECT t FROM Trend t WHERE t.trendId = :trendId"),
    @NamedQuery(name = "Trend.findByInsturmentName", query = "SELECT t FROM Trend t WHERE t.insturmentName = :insturmentName"),
    @NamedQuery(name = "Trend.findByBias", query = "SELECT t FROM Trend t WHERE t.bias = :bias"),
    @NamedQuery(name = "Trend.findByTimeframeInMinutes", query = "SELECT t FROM Trend t WHERE t.timeframeInMinutes = :timeframeInMinutes"),
    @NamedQuery(name = "Trend.findByUpdatedOn", query = "SELECT t FROM Trend t WHERE t.updatedOn = :updatedOn")})
public class Trend implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trend_id")
    private Integer trendId;
    @Column(name = "insturment_name")
    private String insturmentName;
    @Column(name = "bias")
    private String bias;
    @Column(name = "timeframe_in_minutes")
    private Integer timeframeInMinutes;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public Trend() {
    }

    public Trend(Integer trendId) {
        this.trendId = trendId;
    }

    public Integer getTrendId() {
        return trendId;
    }

    public void setTrendId(Integer trendId) {
        this.trendId = trendId;
    }

    public String getInsturmentName() {
        return insturmentName;
    }

    public void setInsturmentName(String insturmentName) {
        this.insturmentName = insturmentName;
    }

    public String getBias() {
        return bias;
    }

    public void setBias(String bias) {
        this.bias = bias;
    }

    public Integer getTimeframeInMinutes() {
        return timeframeInMinutes;
    }

    public void setTimeframeInMinutes(Integer timeframeInMinutes) {
        this.timeframeInMinutes = timeframeInMinutes;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trendId != null ? trendId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trend)) {
            return false;
        }
        Trend other = (Trend) object;
        if ((this.trendId == null && other.trendId != null) || (this.trendId != null && !this.trendId.equals(other.trendId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Trend[ trendId=" + trendId + " ]";
    }
    
}
