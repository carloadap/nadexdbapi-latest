/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.model.ADRMapping;
import com.granberg.model.Adr;
import com.granberg.model.Bias;
import com.granberg.model.OrderType;
import com.granberg.model.Market;
import com.granberg.model.InstrumentType;
import com.granberg.model.Instrument;
import com.granberg.model.News;
import com.granberg.model.Contract;
import com.granberg.model.ContractType;
import com.granberg.model.DelayValue;
import com.granberg.model.EntryOrderType;
import com.granberg.model.Forex;
import com.granberg.model.ForexPeriod;
import com.granberg.model.HourlyPips;
import com.granberg.model.InstrumentMapping;
import com.granberg.model.InstrumentPriceLog;
import com.granberg.model.MarketData;
import com.granberg.model.MarketDataLog;
import com.granberg.model.MonitorTicket;
import com.granberg.model.NadexContext;
import com.granberg.model.NadexCurrency;
import com.granberg.model.NewsRecipient;
import com.granberg.model.Periodicity;
import com.granberg.model.Position;
import com.granberg.model.SecurityStatus;
import com.granberg.model.Session;
import com.granberg.model.SessionAdr;
import com.granberg.model.Ticket;
import com.granberg.model.Trade;
import com.granberg.model.TradeMonitor;
import com.granberg.model.Trend;
import java.math.BigDecimal;
import java.util.Date;

import java.util.List;

/**
 *
 * @author Carlo
 */
public interface IDBAccess {

    /**
     * Methods.
     *
     * @return
     */
    List<Market> findMarket();

    List<Instrument> findInstrument();

    List<InstrumentType> findInstrumentType();

    List<OrderType> findOrderType();

    List<News> findNews();

    List<News> findSameDateTimeNews(Date newsDate);

    List<Contract> findContracts();

    List<Ticket> findTickets();

    List<Position> findPositions();

    List<Periodicity> findPeriodicity();

    List<Date> findDistinctNews();

    List<String> findDistinctEvent();

    DelayValue findDelayValue(int valueID);

    int countNews();

    List<News> findNewsByWithinPeriod(Date date, Date endDate);

    List<NewsRecipient> findRecipients();

    List<NewsRecipient> findActiveRecipients(boolean active);

    void updateInstrumentPrice(String name, BigDecimal price);

    List<News> findNewsByCurrency(String currency);

    void createSecurityStatus(SecurityStatus securityStatus);

    void createNadexContext(NadexContext nadexContext);

    void createContract(Contract contract);

    void createContractCurrencyOnly(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId, int periodicityId);

    void createContractBullSpreadOnly(Contract[] contract, int contractTypeId);

    void createContractCurrencyOnlyNoPeriodicity(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId);

    void createMarketData(MarketData marketData, String namedQuery);

    void createInstrumentPriceLog(InstrumentPriceLog[] instrumentPriceLog);

    void createNews(News news);

    void updateSettledContract(String contractSymbol);

    void updateMarketData(MarketData marketData, int columnToUpdate);

    void createMarketDataLog(MarketDataLog marketDataLog);

    List<Contract> findContractListBySymbolAndExpirationDate(Contract[] contract);

    ContractType findContractTypeByName(String name);

    InstrumentMapping findInstrumentByAlternateName(String name);

    Instrument findInstrumentByName(String name);

    Periodicity findPeriodicityByNadexCode(String name);

    Contract findContractBySymbol(String symbol);

    Contract findContractByIdNotExpired(int contractId, Date date);

    Contract findContractBySymbolNotExpired(String contractSymbol, Date date);

    Contract findContractById(int contractId);

    Instrument findIntrumentById(int id);

    MarketData findMarketDataByContractId(int contractId);

    MarketData findMarketDataByContractSymbol(String contractSymbol);

    List<NadexCurrency> findNadexCurrencyContainsName(String name);

    List<Contract> findContractListBySymbol(String name);

    List<Contract> findContractListNotExpired();

    List<News> findSameNews(String currency, String events, String importance, Date date);

    List<HourlyPips> getHourlyPips(String instrumentName, Integer movingAverageInDays, Integer hourlyInterval);

    List<Contract> findContractByInstrumentIdAndActive(int instrumentId);

    List<Contract> findContractByCotractTypeAndInstrumentId(int contractType, int instrumentId);

    List<MarketData> getMarketDataList();

    Date getOldMarketDataLog(String symbol, int priceUpdated);

    void updateTrend(Trend trend);

    List<Trend> findAllTrends();

    boolean findNearestNewsForSpecificInstrument(String instrumentName);

    Contract findContractByStrikePriceAndExpirationDate(BigDecimal strikePrice, Date expirationDate);

    Contract findContractByStrikePriceAndPeriodicityID(BigDecimal strikePrice, int periodicityID);

    List<Contract> findContractByStrikePrice(BigDecimal strikePrice);

    InstrumentPriceLog findPriceLogLatestByInstrumentName(String instrumentName, int periodId, Date date);

    void UpdateInstrumentHistoryByLastDateAndInstrumentName(String instrumentName, BigDecimal close, BigDecimal high, BigDecimal low);

    void createInstrumentHistory(InstrumentPriceLog instrumentPriceLog);

    Date instrumentHistoryLastLog(String instrumentName);

    List<Forex> findNForexBySymbol(String symbol, int limit);

    List<Forex> findForexBySymbol(String symbol);

    List<InstrumentPriceLog> findInstrumentPriceLogBySymbol(String symbol, int periodId, Date date, int maxResult);

    List<InstrumentPriceLog> findInstrumentPriceLogBySymbol(String symbol, int periodId, Date date);

    ForexPeriod findForexPeriodById(int id);

    ForexPeriod findForexPeriodByCode(String code);

    InstrumentPriceLog findInstrumentPriceLogByInsturmentNameAndClose(String symbol, int periodId, Date date);

    Adr findADRByInstrumentName(String instrumentName);

    ADRMapping findADRMappingByInstrumentName(String instrumentName);

    List<Adr> SelectAllAdr(Date date, int sessionId);
    
    Adr SelectSpecificAdr(String instrumentName, Date date, int sessionId);

    void createADR(Adr[] adr);
    
    void createTrade(Trade[] trade);
    
    void createTradeMonitor(TradeMonitor[] tradeMonitorList);

    void createFXCMPriceLog(InstrumentPriceLog[] instrumentPriceLog);

    void createTicket(Ticket[] ticket);

    void createMonitorTicket(MonitorTicket[] monitorTicket, Date date);

    void updateFXCMPriceLog(InstrumentPriceLog[] instrumentPriceLog);

    void updateAdrHighLow(Adr adr);

    void updateAdr(Adr adr);

    Date findSessionAdrMaxDate(int sessionId);

    Date findAdrMaxDate();

    Date findInstrumentPriceLogMaxDate();

    Date findInstrumentMaxDate();

    Session findSessionById(int sessionId);

    Date findInstrumentPriceLogMaxDateByPeriodType(int periodId);

    List<SessionAdr> findSessionAdrListByInstrumentNameAndSessionId(String instrumentName, int sessionId);

    List<MarketData> findMarketDataBidOfferEntryNotNull();

    List<MarketData> findMarketDataBidOfferEntryNotNullByContractSymbol(String contractSymbol);

    MonitorTicket findMonitorTicketByTicketId(int ticketId);

    Ticket findTicketbyTicketId(int ticketId);

    Ticket findTicketByContractId(int contractId);

    List<MonitorTicket> findAllMonitorTickets();

    List<Ticket> findAllTickets();

    List<Ticket> findAllTicketsNotExpired(String statusId);

    void updateMonitorTicket(MonitorTicket monitorTicket, Instrument instrument, Date date);

    void updateMonitorTicketTest(MonitorTicket monitorTicket, MarketData marketData, Ticket ticket, Date date, double multiplier);

    void updateTicket(Ticket[] ticket);

    void updateTicket(Ticket ticket);

    Bias findBiasByEmail(String email);
    
    void deleteContract(Contract contract);
    
    void updateContract(Contract contract, int contractTypeId);
    
    List<InstrumentPriceLog> findByInstrumentNameAndPeriodType(String instrumentName, int periodId);
    
    //delete soon
    boolean findIfTradeExist(Date date, int sessionId);
    
    boolean findIfTradeMonitorExist(Date date);
    
    EntryOrderType findEntryOrderTypeByEntryTypeId(int entryTypeId);
    
    List<TradeMonitor> findAllTradeMonitor();
    
    void updateTradeMonitor(TradeMonitor tradeMonitor);
}
