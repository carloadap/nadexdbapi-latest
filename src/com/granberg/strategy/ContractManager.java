/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.Contract;
import com.granberg.model.Instrument;
import java.awt.Event;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Carlo
 */
public class ContractManager {

    IDBAccess dBAccess = MySQLDBAccess.getInstance();
    Event OnContractCloseEvent;
    private static ContractManager instance;
    private int contractTypeID = 1;
    private double pips = 0.00;
    private int periodicityID = 1;

    public static ContractManager getInstance() {
        if (instance == null) {
            instance = new ContractManager();
        }
        return instance;
    }

    public void OnContractOpen() {
    }

    public void OnContractClose() {
    }

    public Contract OnPriceCrossesContract(Instrument intrument, long diffInCurrentTimeInMinutes) {
//        long diff = contract.getExpirationDate().getTime() - contract.getOpeningDate().getTime();
//        long diffInMinutes = diff / (1000 * 60);
//        double allowTimePercentToTrade = diffInMinutes * 0.6;
//        boolean trade = false;
//        if (diffInCurrentTimeInMinutes > allowTimePercentToTrade) {
//            System.out.println("TRADE");
//            trade = true;
//        }

//        Contract contract = dBAccess.findContractBetweenPips(instrument.getInstrumentPrice());
        return null;
    }

    public long getTimeLeftForContactInMinutes(Contract contract) {
        Date currentTime = new Date();
        long diffInCurrentTime = contract.getExpirationDate().getTime() - currentTime.getTime();
        long diffInCurrentTimeInMinutes = diffInCurrentTime / (1000 * 60);
        return diffInCurrentTimeInMinutes;
    }

    public double getPercentageRemainingTime(Contract contract, long diffInCurrentTime) {
        long diff = contract.getExpirationDate().getTime() - contract.getOpeningDate().getTime();
        long diffInMinutes = diff / (1000 * 60);
        double percentage = (double) diffInCurrentTime / diffInMinutes;
        return percentage;
    }

    public List<Contract> getNearestActiveContract(Instrument instrument) {
        List<Contract> contractList = dBAccess.findContractByCotractTypeAndInstrumentId(contractTypeID, instrument.getInstrumentId());
        double distance = Double.MAX_VALUE;
//        Contract activeContract = null;
        double strikePrice = Double.MAX_VALUE;

        for (Contract contract : contractList) {
            double cdistance = Math.abs(contract.getStrikePrice().doubleValue() - instrument.getInstrumentPrice().doubleValue());
            if (cdistance < distance) {
                strikePrice = Double.valueOf(contract.getStrikePrice().doubleValue());
//                activeContract = contract;
                distance = cdistance;
            }
        }
        List<Contract> activeContractList = dBAccess.findContractByStrikePrice(BigDecimal.valueOf(strikePrice));
        return activeContractList;
    }

    public Contract getCheapestContract(Contract contract) {
        BigDecimal strikePrice = contract.getStrikePrice();
        Date expirationDate = contract.getExpirationDate();
//        System.out.println(strikePrice);
//        System.out.println(expirationDate);
        double newStrikePrice = 0.80;
        BigDecimal newPrice = strikePrice.add(BigDecimal.valueOf(newStrikePrice));
//        System.out.println(newPrice);
        Contract cheapestContract = dBAccess.findContractByStrikePriceAndExpirationDate(newPrice, expirationDate);
//        System.out.println(cheapestContract.getContractDescription());
        return cheapestContract;
    }

    public Instrument getCurrentContractPrice(Contract contract) {
        Instrument instrument = dBAccess.findIntrumentById(contract.getContractId());
        return instrument;
    }
    
    public Contract getAboveBelowContracts(Contract contract, boolean isBuy){
        if(isBuy){
            return dBAccess.findContractByStrikePriceAndPeriodicityID(contract.getStrikePrice().add(BigDecimal.valueOf(pips)), periodicityID);
        }else{
            return dBAccess.findContractByStrikePriceAndPeriodicityID(contract.getStrikePrice().subtract(BigDecimal.valueOf(pips)), periodicityID);
        }
    }

    /**
     * @return the contractTypeID
     */
    public int getContractTypeID() {
        return contractTypeID;
    }

    /**
     * @param contractTypeID the contractTypeID to set
     */
    public void setContractTypeID(int contractTypeID) {
        this.contractTypeID = contractTypeID;
    }

    /**
     * @return the strikePrice
     */
    public double getPips() {
        return pips;
    }

    /**
     * @param strikePrice the strikePrice to set
     */
    public void setPips(double pips) {
        this.pips = pips;
    }

    /**
     * @return the periodicityID
     */
    public int getPeriodicityID() {
        return periodicityID;
    }

    /**
     * @param periodicityID the periodicityID to set
     */
    public void setPeriodicityID(int periodicityID) {
        this.periodicityID = periodicityID;
    }
}
