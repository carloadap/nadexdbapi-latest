///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.granberg.strategy;
//
//import com.granberg.IDBAccess;
//import com.granberg.MySQLDBAccess;
//import com.granberg.model.BollingerBand;
//import com.granberg.model.Forex;
//import java.util.List;
//import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
//
///**
// *
// * @author Carlo
// */
//public class Bollinger {
//
//    private static Bollinger instance;
//
//    public static Bollinger getInstance() {
//        if (instance == null) {
//            instance = new Bollinger();
//        }
//        return instance;
//    }
//
//    public void processForexHistory(List<Forex> forexList) {
//        BollingerManager bb = BollingerManager.getInstance();
//        int day = bb.getDay();
//        int count = bb.getCount();
//        double SUM = 0.00;
//        List<Double> EMAList = bb.getEMAList();
//        for (int i = 0; i < 10; i++) {
//            Forex forex = forexList.get(i);
//            SUM += forex.getClose().doubleValue();
//            if (count == 0) {
//                EMAList.add(forex.getClose().doubleValue());
//            }
//        }
//        bb.setSUM(SUM);
//        bb.setEMAList(EMAList);
//    }
//
//    public double[] convertEMAListToArray(List<Double> EMAList) {
//        double[] EMAArray = new double[EMAList.size()];
//        for (int i = 0; i < EMAArray.length; i++) {
//            EMAArray[i] = EMAList.get(i);
//        }
//        return EMAArray;
//    }
//
//    public double getEMADeviation(double[] forexHistoryArray) {
//        StandardDeviation deviation = new StandardDeviation();
//        return deviation.evaluate(forexHistoryArray);
//    }
//
//    public double getSMA(double sumOfClosePrice) {
//        BollingerManager bollingerManager = BollingerManager.getInstance();
//        int day = bollingerManager.getDay();
//        return sumOfClosePrice / day;
//    }
//
//    public double getEMA(double lastClosePrice, double SMA) {
//        BollingerManager bollingerManager = BollingerManager.getInstance();
//        int count = bollingerManager.getCount();
//        double multiplier = bollingerManager.getMultiplier();
//        double EMA = bollingerManager.getOldEMA();
//        if (count == 0) {
//            EMA = SMA;
//        } else {
//            EMA = multiplier * (lastClosePrice - EMA) + EMA;
//            bollingerManager.getEMAList().remove(0);
//            bollingerManager.getEMAList().add(EMA);
//        }
////                    multiplier * (lastClosePrice - EMA) + EMA;
//        bollingerManager.setOldEMA(EMA);
//        return EMA;
//    }
//
//    public double getHBAND(double EMA, double EMADeviation) {
//        double HBAND = EMA + (EMADeviation) * 2.2;
//        return HBAND;
//    }
//
//    public double getLBAND(double EMA, double EMADeviation) {
//        double LBAND = EMA - (EMADeviation) * 2.2;
//        return LBAND;
//    }
//}
