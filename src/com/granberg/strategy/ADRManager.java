/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.model.InstrumentPriceLog;
import com.granberg.model.SessionAdr;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Carlo
 */
public class ADRManager {

    private static ADRManager instance;
//    private int period = 10;
    List<InstrumentPriceLog> forexListPeriod = new LinkedList<>();
    private int[] periods = {5};

    public static ADRManager getInstance() {
        if (instance == null) {
            instance = new ADRManager();
        }
        return instance;
    }

    public double evaluate(List<InstrumentPriceLog> forexList) {
//        double trueRange = getSimpleAverage(forexList);
//        double firstRange = getFirstRange(forexList);
//        double secondRange = getSecondRange(forexList);
//        double ADR = (trueRange + firstRange + secondRange) / 3;
        double range = 0.00;
        Collections.sort(forexList, new Comparator<InstrumentPriceLog>() {
            public int compare(InstrumentPriceLog m1, InstrumentPriceLog m2) {
                return m1.getDate().compareTo(m2.getDate());
            }
        });
        for (int period : periods) {
            range += getRange(forexList, period);
        }
        double apr = range / periods.length;
        return apr;
    }

    public double getADRHigh(double adr, double open) {
        return open + adr;
    }

    public double getADRLow(double adr, double open) {
        return open - adr;
    }

    public double convertToPips(double ADR) {
        return ADR * 100;
    }

    public double getSimpleAverage(List<InstrumentPriceLog> forexList) {
        double trueRange = 0.00;
        for (InstrumentPriceLog forex : forexList) {
            trueRange += (forex.getHigh().doubleValue() - forex.getLow().doubleValue());
        }
        trueRange = trueRange / forexList.size();
        return trueRange;
    }

    public double getFirstRange(List<InstrumentPriceLog> forexList) {
        double firstTrueRange = 0.00;
        double secondTrueRange = 0.00;
        int firstTrueRangePosition = forexList.size() - 1;
        int secondTrueRangePosition = forexList.size() - 2;
        InstrumentPriceLog forexFirstRange = forexList.get(firstTrueRangePosition);
        InstrumentPriceLog forexSecondRange = forexList.get(secondTrueRangePosition);
        firstTrueRange = forexFirstRange.getHigh().doubleValue() - forexFirstRange.getLow().doubleValue();
        secondTrueRange = forexSecondRange.getHigh().doubleValue() - forexSecondRange.getLow().doubleValue();
        return (firstTrueRange + secondTrueRange) / 2;
    }

    public double getRange(List<InstrumentPriceLog> forexList, int period) {
        double range = 0.00;
        for (int i = 1; i <= period; i++) {
            InstrumentPriceLog forex = forexList.get(forexList.size() - i);
            System.out.println(String.format("CURRENCY : %s, HIGH: %s, LOW: %s, DATE: %s", forex.getInstrumentName(), forex.getHigh(), forex.getLow(), forex.getDate()));
            range += (forex.getHigh().doubleValue() - forex.getLow().doubleValue());
        }
        double totalRange = range / period;
        return totalRange;
    }

//    /**
//     * @return the period
//     */
//    public int getPeriod() {
//        return period;
//    }
//
//    /**
//     * @param period the period to set
//     */
//    public void setPeriod(int period) {
//        this.period = period;
//    }
    /**
     * @return the periods
     */
    public int[] getPeriods() {
        return periods;
    }

    /**
     * @param periods the periods to set
     */
    public void setPeriods(int[] periods) {
        this.periods = periods;
    }
}
