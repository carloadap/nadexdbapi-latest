/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.strategy;

import com.granberg.model.BollingerBand;
import com.granberg.model.InstrumentPriceLog;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

/**
 *
 * @author Carlo
 */
public class BollingerManager {

    private static BollingerManager instance;
    private int period = 10;
    private double deviation;
    private int count = 0;
    private double SUM;
    private List<Double> EMAList = new LinkedList<>();
    private double oldEMA;
    private double EMADeviation;
    private double[] EMAArray;
    private List<BollingerBand> bollingerBandList = new LinkedList<>();

    public static BollingerManager getInstance() {
        if (instance == null) {
            instance = new BollingerManager();
        }
        return instance;
    }

    public List<BollingerBand> evaluate(List<InstrumentPriceLog> forexHistory) {
        while (forexHistory.size() >= period) {
            System.out.println(forexHistory.size());
            BollingerBand bollingerBand = new BollingerBand();
            processForexHistory(forexHistory);
            InstrumentPriceLog forex = forexHistory.get(period - 1);
            bollingerBand.setSMA(getSMA(SUM));
            bollingerBand.setEMA(getEMA(forex.getClose().doubleValue(), bollingerBand.getSMA()));
            EMAArray = convertEMAListToArray(EMAList);
            EMADeviation = getEMADeviation(EMAArray);
            bollingerBand.setHBAND(getHBAND(oldEMA, EMADeviation));
            bollingerBand.setLBAND(getLBAND(oldEMA, EMADeviation));
            System.out.println(String.format("SMA: %s, EMA: %s", bollingerBand.getSMA(), bollingerBand.getEMA()));
            count++;
            forexHistory.remove(0);
            bollingerBandList.add(bollingerBand);
        }
        return bollingerBandList;
    }

    public void processForexHistory(List<InstrumentPriceLog> forexList) {
        double sumOfClosePrices = 0.00;
        for (int i = 0; i < period; i++) {
            InstrumentPriceLog forex = forexList.get(i);
            System.out.println(String.format("Symbol : %s, Price : %s, Date : %s ", forex.getInstrumentName(), forex.getClose(), forex.getDate()));
            sumOfClosePrices += forex.getClose().doubleValue();
            if (count == 0) {
                EMAList.add(forex.getClose().doubleValue());
            }
        }
        SUM = sumOfClosePrices;
    }

    public double[] convertEMAListToArray(List<Double> EMAList) {
        EMAArray = new double[EMAList.size()];
        for (int i = 0; i < EMAArray.length; i++) {
            EMAArray[i] = EMAList.get(i);
        }
        return EMAArray;
    }

    public double getEMADeviation(double[] forexHistoryArray) {
        StandardDeviation standardDeviation = new StandardDeviation();
        return standardDeviation.evaluate(forexHistoryArray);
    }

    public double getSMA(double sumOfClosePrice) {
        return sumOfClosePrice / getPeriod();
    }

    public double getEMA(double lastClosePrice, double SMA) {
        if (count == 0) {
            oldEMA = SMA;
            EMAList.remove(EMAList.size() - 1);
            EMAList.add(oldEMA);
        } else {
            oldEMA = getDeviation() * (lastClosePrice - oldEMA) + oldEMA;
            EMAList.remove(0);
            EMAList.add(oldEMA);
        }
//                    multiplier * (lastClosePrice - EMA) + EMA;
        return oldEMA;
    }

    public double getHBAND(double EMA, double EMADeviation) {
        double HBAND = EMA + (EMADeviation) * 2.2;
        return HBAND;
    }

    public double getLBAND(double EMA, double EMADeviation) {
        double LBAND = EMA - (EMADeviation) * 2.2;
        return LBAND;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the SUM
     */
    public double getSUM() {
        return SUM;
    }

    /**
     * @param SUM the SUM to set
     */
    public void setSUM(double SUM) {
        this.SUM = SUM;
    }

    /**
     * @return the EMAList
     */
    public List<Double> getEMAList() {
        return EMAList;
    }

    /**
     * @param EMAList the EMAList to set
     */
    public void setEMAList(List<Double> EMAList) {
        this.EMAList = EMAList;
    }

    /**
     * @return the oldEMA
     */
    public double getOldEMA() {
        return oldEMA;
    }

    /**
     * @param oldEMA the oldEMA to set
     */
    public void setOldEMA(double oldEMA) {
        this.oldEMA = oldEMA;
    }

    /**
     * @return the EMADeviation
     */
    public double getEMADeviation() {
        return EMADeviation;
    }

    /**
     * @param EMADeviation the EMADeviation to set
     */
    public void setEMADeviation(double EMADeviation) {
        this.EMADeviation = EMADeviation;
    }

    /**
     * @return the EMAArray
     */
    public double[] getEMAArray() {
        return EMAArray;
    }

    /**
     * @param EMAArray the EMAArray to set
     */
    public void setEMAArray(double[] EMAArray) {
        this.EMAArray = EMAArray;
    }

    /**
     * @return the period
     */
    public int getPeriod() {
        return period;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(int period) {
        this.period = period;
    }

    /**
     * @return the deviation
     */
    public double getDeviation() {
        deviation = 2.00 / (period + 1.00);
        return deviation;
    }
}
