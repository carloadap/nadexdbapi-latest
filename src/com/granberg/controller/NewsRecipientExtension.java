/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.model.News;
import com.granberg.model.NewsRecipient;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class NewsRecipientExtension extends NewsRecipientJpaController {

    public NewsRecipientExtension(EntityManagerFactory emf) {
        super(emf);
    }

    public List<NewsRecipient> findActiveNewsRecipients(boolean active) {
        EntityManager em;
        em = this.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<NewsRecipient> criteria = builder.createQuery(NewsRecipient.class);
        Root<NewsRecipient> recipientRoot = criteria.from(NewsRecipient.class);
        criteria.select(recipientRoot);
        criteria.where(builder.equal(recipientRoot.get("active"), active));
        List<NewsRecipient> recipientList = em.createQuery(criteria).getResultList();
        return recipientList;
    }
}
