/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.ForexPeriod;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class ForexPeriodJpaController implements Serializable {

    public ForexPeriodJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ForexPeriod forexPeriod) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(forexPeriod);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ForexPeriod forexPeriod) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            forexPeriod = em.merge(forexPeriod);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = forexPeriod.getForexPeriodId();
                if (findForexPeriod(id) == null) {
                    throw new NonexistentEntityException("The forexPeriod with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ForexPeriod forexPeriod;
            try {
                forexPeriod = em.getReference(ForexPeriod.class, id);
                forexPeriod.getForexPeriodId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The forexPeriod with id " + id + " no longer exists.", enfe);
            }
            em.remove(forexPeriod);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ForexPeriod> findForexPeriodEntities() {
        return findForexPeriodEntities(true, -1, -1);
    }

    public List<ForexPeriod> findForexPeriodEntities(int maxResults, int firstResult) {
        return findForexPeriodEntities(false, maxResults, firstResult);
    }

    private List<ForexPeriod> findForexPeriodEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ForexPeriod.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public ForexPeriod findForexPeriodByCode(String code) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("ForexPeriod.findByCode", ForexPeriod.class);
            query.setParameter("code", code);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (ForexPeriod) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public ForexPeriod findForexPeriod(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ForexPeriod.class, id);
        } finally {
            em.close();
        }
    }

    public int getForexPeriodCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ForexPeriod> rt = cq.from(ForexPeriod.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
