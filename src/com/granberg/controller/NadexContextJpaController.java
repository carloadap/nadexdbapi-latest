/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.controller.exceptions.PreexistingEntityException;
import com.granberg.model.NadexContext;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class NadexContextJpaController implements Serializable {

    public NadexContextJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NadexContext nadexContext) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nadexContext);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNadexContext(nadexContext.getNadexContextId()) != null) {
                throw new PreexistingEntityException("NadexContext " + nadexContext + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NadexContext nadexContext) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            nadexContext = em.merge(nadexContext);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = nadexContext.getNadexContextId();
                if (findNadexContext(id) == null) {
                    throw new NonexistentEntityException("The nadexContext with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NadexContext nadexContext;
            try {
                nadexContext = em.getReference(NadexContext.class, id);
                nadexContext.getNadexContextId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nadexContext with id " + id + " no longer exists.", enfe);
            }
            em.remove(nadexContext);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NadexContext> findNadexContextEntities() {
        return findNadexContextEntities(true, -1, -1);
    }

    public List<NadexContext> findNadexContextEntities(int maxResults, int firstResult) {
        return findNadexContextEntities(false, maxResults, firstResult);
    }

    private List<NadexContext> findNadexContextEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NadexContext.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NadexContext findNadexContext(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NadexContext.class, id);
        } finally {
            em.close();
        }
    }

    public int getNadexContextCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NadexContext> rt = cq.from(NadexContext.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
