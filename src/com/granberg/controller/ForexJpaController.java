/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Forex;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class ForexJpaController implements Serializable {

    public ForexJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Forex forex) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(forex);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Forex forex) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            forex = em.merge(forex);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = forex.getForexId();
                if (findForex(id) == null) {
                    throw new NonexistentEntityException("The forex with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Forex forex;
            try {
                forex = em.getReference(Forex.class, id);
                forex.getForexId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The forex with id " + id + " no longer exists.", enfe);
            }
            em.remove(forex);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Forex> findForexEntities() {
        return findForexEntities(true, -1, -1);
    }

    public List<Forex> findForexEntities(int maxResults, int firstResult) {
        return findForexEntities(false, maxResults, firstResult);
    }

    private List<Forex> findForexEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Forex.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Forex> findNForexBySymbol(String symbol, int limit) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Forex.findNBySymbol", Forex.class);
            query.setParameter("symbol", symbol);
            query.setFirstResult(0);
            query.setMaxResults(limit);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Forex>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }
    
    public List<Forex> findForexBySymbol(String symbol) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Forex.findBySymbol", Forex.class);
            query.setParameter("symbol", symbol);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Forex>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Forex findForex(Integer id) {
        EntityManager em = getEntityManager();




        try {
            return em.find(Forex.class, id);
        } finally {
            em.close();
        }
    }

    public int getForexCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Forex> rt = cq.from(Forex.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
