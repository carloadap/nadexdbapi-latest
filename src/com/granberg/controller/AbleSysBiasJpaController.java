/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.AbleSysBias;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class AbleSysBiasJpaController implements Serializable {

    public AbleSysBiasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AbleSysBias ableSysBias) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ableSysBias);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AbleSysBias ableSysBias) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ableSysBias = em.merge(ableSysBias);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ableSysBias.getBiasId();
                if (findAbleSysBias(id) == null) {
                    throw new NonexistentEntityException("The ableSysBias with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AbleSysBias ableSysBias;
            try {
                ableSysBias = em.getReference(AbleSysBias.class, id);
                ableSysBias.getBiasId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ableSysBias with id " + id + " no longer exists.", enfe);
            }
            em.remove(ableSysBias);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AbleSysBias> findAbleSysBiasEntities() {
        return findAbleSysBiasEntities(true, -1, -1);
    }

    public List<AbleSysBias> findAbleSysBiasEntities(int maxResults, int firstResult) {
        return findAbleSysBiasEntities(false, maxResults, firstResult);
    }

    private List<AbleSysBias> findAbleSysBiasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AbleSysBias.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AbleSysBias findAbleSysBias(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AbleSysBias.class, id);
        } finally {
            em.close();
        }
    }

    public int getAbleSysBiasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AbleSysBias> rt = cq.from(AbleSysBias.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
