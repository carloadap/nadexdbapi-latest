/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.ADRMapping;
import com.granberg.model.Instrument;
import com.granberg.model.MarketData;
import com.granberg.model.MonitorTicket;
import com.granberg.model.Ticket;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class MonitorTicketJpaController implements Serializable {

    public MonitorTicketJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MonitorTicket monitorTicket) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(monitorTicket);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void createMonitorTicket(MonitorTicket[] monitorTicket, Date date) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (MonitorTicket record : monitorTicket) {
                Query query = em.createNamedQuery("MonitorTicket.findByTicketId", MonitorTicket.class);
                query.setParameter("ticketId", record.getTicketId());
                List list = query.getResultList();
                if (list.isEmpty()) {
                    em.persist(record);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateMonitorTicketTest(MonitorTicket monitorTicket, MarketData marketData, Ticket ticket, Date date, double multiplier) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("MonitorTicket.findByTicketId", MonitorTicket.class);
            query.setParameter("ticketId", monitorTicket.getTicketId());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                MonitorTicket oldMonitorTicket = (MonitorTicket) query.getSingleResult();
                if (ticket.getPositionId() == 0) {
                    oldMonitorTicket.setCurrentPrice(marketData.getOffer());
                    oldMonitorTicket.setUpdatedOn(date);
                    double totalProfit = (ticket.getPrice().doubleValue() - marketData.getOffer().doubleValue()) * multiplier;
                    if (totalProfit < ticket.getMaxLoss().negate().doubleValue()) {
                        oldMonitorTicket.setTotalProfit(ticket.getMaxLoss().negate());
                    } else {
                        oldMonitorTicket.setTotalProfit(BigDecimal.valueOf(totalProfit));
                    }

                    if (marketData.getOffer().doubleValue() > oldMonitorTicket.getHighestPrice().doubleValue()) {
                        oldMonitorTicket.setHighestPrice(marketData.getOffer());
                        oldMonitorTicket.setHighestPriceDate(date);
                    }
                    if (marketData.getOffer().doubleValue() < oldMonitorTicket.getLowestPrice().doubleValue()) {
                        oldMonitorTicket.setLowestPrice(marketData.getOffer());
                        oldMonitorTicket.setLowestPriceDate(date);
                    }
                } else {

                    oldMonitorTicket.setCurrentPrice(marketData.getBid());
                    oldMonitorTicket.setUpdatedOn(date);
                    double totalProfit = (marketData.getBid().doubleValue() - marketData.getOffer().doubleValue()) * multiplier;
                    if (totalProfit < ticket.getMaxLoss().negate().doubleValue()) {
                        oldMonitorTicket.setTotalProfit(ticket.getMaxLoss().negate());
                    } else {
                        oldMonitorTicket.setTotalProfit(BigDecimal.valueOf(totalProfit));
                    }
                    if (marketData.getBid().doubleValue() > oldMonitorTicket.getHighestPrice().doubleValue()) {
                        oldMonitorTicket.setHighestPrice(marketData.getBid());
                        oldMonitorTicket.setHighestPriceDate(date);
                    }
                    if (marketData.getBid().doubleValue() < oldMonitorTicket.getLowestPrice().doubleValue()) {
                        oldMonitorTicket.setLowestPrice(marketData.getBid());
                        oldMonitorTicket.setLowestPriceDate(date);
                    }
//                    
//                    oldMonitorTicket.setCurrentPrice(marketData.getBid());
//                    oldMonitorTicket.setUpdatedOn(date);
//                    double totalProfit = (marketData.getOffer().doubleValue() - marketData.getBid().doubleValue()) * multiplier;
//                    if (totalProfit < ticket.getMaxLoss().doubleValue() ) {
//                        oldMonitorTicket.setTotalProfit(ticket.getMaxLoss().negate());
//                    } else {
//                        oldMonitorTicket.setTotalProfit(BigDecimal.valueOf(totalProfit));
//                    }
//                    if (marketData.getBid().doubleValue() > oldMonitorTicket.getHighestPrice().doubleValue()) {
//                        oldMonitorTicket.setHighestPrice(marketData.getBid());
//                        oldMonitorTicket.setHighestPriceDate(date);
//                    }
//                    if (marketData.getBid().doubleValue() < oldMonitorTicket.getLowestPrice().doubleValue()) {
//                        oldMonitorTicket.setLowestPrice(marketData.getBid());
//                        oldMonitorTicket.setLowestPriceDate(date);
//                    }
                }
                em.getTransaction().commit();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateMonitorTicket(MonitorTicket monitorTicket, Instrument instrument, Date date) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("MonitorTicket.findByTicketId", MonitorTicket.class);
            query.setParameter("ticketId", monitorTicket.getTicketId());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                MonitorTicket oldMonitorTicket = (MonitorTicket) query.getSingleResult();
                if (oldMonitorTicket.getHighestPrice().doubleValue() < instrument.getInstrumentPrice().doubleValue()) {
                    oldMonitorTicket.setHighestPrice(instrument.getInstrumentPrice());
                    oldMonitorTicket.setHighestPriceDate(date);
                    oldMonitorTicket.setUpdatedOn(date);
                }
                if (oldMonitorTicket.getLowestPrice().doubleValue() > instrument.getInstrumentPrice().doubleValue()) {
                    oldMonitorTicket.setLowestPrice(instrument.getInstrumentPrice());
                    oldMonitorTicket.setLowestPriceDate(date);
                    oldMonitorTicket.setUpdatedOn(date);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MonitorTicket monitorTicket) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            monitorTicket = em.merge(monitorTicket);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = monitorTicket.getMonitorId();
                if (findMonitorTicket(id) == null) {
                    throw new NonexistentEntityException("The monitorTicket with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MonitorTicket monitorTicket;
            try {
                monitorTicket = em.getReference(MonitorTicket.class, id);
                monitorTicket.getMonitorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The monitorTicket with id " + id + " no longer exists.", enfe);
            }
            em.remove(monitorTicket);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MonitorTicket> findMonitorTicketEntities() {
        return findMonitorTicketEntities(true, -1, -1);
    }

    public List<MonitorTicket> findMonitorTicketEntities(int maxResults, int firstResult) {
        return findMonitorTicketEntities(false, maxResults, firstResult);
    }

    private List<MonitorTicket> findMonitorTicketEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MonitorTicket.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MonitorTicket findMonitorTicket(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MonitorTicket.class, id);
        } finally {
            em.close();
        }
    }

    public int getMonitorTicketCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MonitorTicket> rt = cq.from(MonitorTicket.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
