/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Instrument;
import com.granberg.model.InstrumentPriceLog;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class InstrumentPriceLogJpaController implements Serializable {

    public InstrumentPriceLogJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InstrumentPriceLog instrumentPriceLog) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instrumentPriceLog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InstrumentPriceLog instrumentPriceLog) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            instrumentPriceLog = em.merge(instrumentPriceLog);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = instrumentPriceLog.getInsturmentPriceLogId();
                if (findInstrumentPriceLog(id) == null) {
                    throw new NonexistentEntityException("The instrumentPriceLog with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InstrumentPriceLog instrumentPriceLog;
            try {
                instrumentPriceLog = em.getReference(InstrumentPriceLog.class, id);
                instrumentPriceLog.getInsturmentPriceLogId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrumentPriceLog with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrumentPriceLog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InstrumentPriceLog> findInstrumentPriceLogEntities() {
        return findInstrumentPriceLogEntities(true, -1, -1);
    }

    public List<InstrumentPriceLog> findInstrumentPriceLogEntities(int maxResults, int firstResult) {
        return findInstrumentPriceLogEntities(false, maxResults, firstResult);
    }

    private List<InstrumentPriceLog> findInstrumentPriceLogEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InstrumentPriceLog.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public void createContractCurrencyOnly(InstrumentPriceLog[] instrumentPriceLogList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (InstrumentPriceLog record : instrumentPriceLogList) {
                em.persist(record);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public InstrumentPriceLog findPriceLogLatestByInstrumentName(String instrumentName, int periodId, Date date) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndLastDateAndPeriod", InstrumentPriceLog.class);
            query.setParameter("instrumentName", instrumentName).setParameter("periodType", periodId).setParameter("date", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (InstrumentPriceLog) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<InstrumentPriceLog> findInstrumentHistoryListByInstrumentName(String instrumentName, int periodId, Date date, int maxResult) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndPeriodIdDesc", InstrumentPriceLog.class);
            query.setParameter("instrumentName", instrumentName).setParameter("periodType", periodId).setParameter("date", date);
            query.setMaxResults(maxResult);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<InstrumentPriceLog>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<InstrumentPriceLog> findByInstrumentNameAndPeriodType(String instrumentName, int periodId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndPeriodType", InstrumentPriceLog.class);
            query.setParameter("instrumentName", instrumentName).setParameter("periodType", periodId);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<InstrumentPriceLog>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<InstrumentPriceLog> findInstrumentHistoryListByInstrumentName(String instrumentName, int periodId, Date date) {
        EntityManager em = getEntityManager();
        try {
            Date lastDate = findInstrumentHistoryLastDate(instrumentName);
            if (lastDate != null) {
                Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndPeriodIdDesc", InstrumentPriceLog.class);
                query.setParameter("instrumentName", instrumentName).setParameter("periodType", periodId).setParameter("date", date);
                List list = query.getResultList();
                if (!list.isEmpty()) {
                    return (List<InstrumentPriceLog>) query.getResultList();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public void createPriceLogList(InstrumentPriceLog[] instrumentPriceLogList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (InstrumentPriceLog instrumentPriceLog : instrumentPriceLogList) {
                em.persist(instrumentPriceLog);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updatePriceLogList(InstrumentPriceLog[] instrumentPriceLogList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            for (InstrumentPriceLog instrumentPriceLog : instrumentPriceLogList) {
                Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndLastDateAndPeriod", InstrumentPriceLog.class);
                query.setParameter("instrumentName", instrumentPriceLog.getInstrumentName()).setParameter("date", instrumentPriceLog.getDate()).setParameter("periodType", instrumentPriceLog.getPeriodType());
                List list = query.getResultList();
                if (!list.isEmpty()) {
                    em.getTransaction().begin();
                    InstrumentPriceLog instrumentHistory = (InstrumentPriceLog) query.getSingleResult();
                    instrumentHistory.setClose(instrumentPriceLog.getOpen());
                    if (instrumentPriceLog.getOpen().doubleValue() > instrumentHistory.getHigh().doubleValue()) {
                        instrumentHistory.setHigh(instrumentPriceLog.getOpen());
                    }
                    if (instrumentPriceLog.getOpen().doubleValue() < instrumentHistory.getLow().doubleValue()) {
                        instrumentHistory.setLow(instrumentPriceLog.getOpen());
                    }
                    em.getTransaction().commit();
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public InstrumentPriceLog findByInstrumentNameAndClose(String instrumentName, int periodId, Date date) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndDate", InstrumentPriceLog.class);
            query.setParameter("instrumentName", instrumentName).setParameter("periodType", periodId).setParameter("date", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (InstrumentPriceLog) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Date findInstrumentHistoryLastDate(String instrumentName) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByLastDate", InstrumentPriceLog.class);
//            Query query = em.createStoredProcedureQuery("SELECT MAX(i.date) FROM InstrumentPriceLog i WHERE i.instrumentName = instrumentName");

            query.setParameter("instrumentName", instrumentName);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Date) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public void UpdateInstrumentHistoryByLastDateAndInstrumentName(String instrumentName, BigDecimal close, BigDecimal high, BigDecimal low) {
        EntityManager em = getEntityManager();
        try {
            Date lastDate = findInstrumentHistoryLastDate(instrumentName);
            if (lastDate != null) {
                Query query = em.createNamedQuery("InstrumentPriceLog.findByInstrumentNameAndLastDate", InstrumentPriceLog.class);
                query.setParameter("instrumentName", instrumentName).setParameter("date", lastDate);
                List list = query.getResultList();
                if (!list.isEmpty()) {
                    em.getTransaction().begin();
                    InstrumentPriceLog instrumentHistory = (InstrumentPriceLog) query.getSingleResult();
                    instrumentHistory.setClose(close);
                    instrumentHistory.setHigh(high);
                    instrumentHistory.setLow(low);
                    em.getTransaction().commit();
                }
            }
        } finally {
            em.close();
        }
    }

    public Date findLatestInstrumentPriceLog() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findByLastDateData", InstrumentPriceLog.class);
            Date date = (Date) query.getSingleResult();
            return date;
        } finally {
            em.close();
        }
    }

    public Date findLatestInstrumentPriceLogByPeriodId(int periodId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("InstrumentPriceLog.findLastDateByPeriod", InstrumentPriceLog.class);
            query.setParameter("periodType", periodId);
            Date date = (Date) query.getSingleResult();
            return date;
        } finally {
            em.close();
        }
    }

    public InstrumentPriceLog findInstrumentPriceLog(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InstrumentPriceLog.class, id);
        } finally {
            em.close();
        }
    }

    public int getInstrumentPriceLogCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InstrumentPriceLog> rt = cq.from(InstrumentPriceLog.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
