/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.NewsRecipient;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class NewsRecipientJpaController implements Serializable {

    public NewsRecipientJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NewsRecipient newsRecipient) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(newsRecipient);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NewsRecipient newsRecipient) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            newsRecipient = em.merge(newsRecipient);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = newsRecipient.getRecipientId();
                if (findNewsRecipient(id) == null) {
                    throw new NonexistentEntityException("The newsRecipient with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NewsRecipient newsRecipient;
            try {
                newsRecipient = em.getReference(NewsRecipient.class, id);
                newsRecipient.getRecipientId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The newsRecipient with id " + id + " no longer exists.", enfe);
            }
            em.remove(newsRecipient);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NewsRecipient> findNewsRecipientEntities() {
        return findNewsRecipientEntities(true, -1, -1);
    }

    public List<NewsRecipient> findNewsRecipientEntities(int maxResults, int firstResult) {
        return findNewsRecipientEntities(false, maxResults, firstResult);
    }

    private List<NewsRecipient> findNewsRecipientEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NewsRecipient.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    

    public NewsRecipient findNewsRecipient(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NewsRecipient.class, id);
        } finally {
            em.close();
        }
    }

    public int getNewsRecipientCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NewsRecipient> rt = cq.from(NewsRecipient.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
