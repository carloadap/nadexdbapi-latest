/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.News;
import com.granberg.model.News_;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class NewsJpaController implements Serializable {

    public NewsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(News news) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(news);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(News news) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            news = em.merge(news);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = news.getNewsId();
                if (findNews(id) == null) {
                    throw new NonexistentEntityException("The news with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            News news;
            try {
                news = em.getReference(News.class, id);
                news.getNewsId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The news with id " + id + " no longer exists.", enfe);
            }
            em.remove(news);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<News> findNewsEntities() {
        return findNewsEntities(true, -1, -1);
    }

    public List<News> findNewsEntities(int maxResults, int firstResult) {
        return findNewsEntities(false, maxResults, firstResult);
    }

    private List<News> findNewsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {


            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(News.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public News findNews(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(News.class, id);
        } finally {
            em.close();
        }
    }

    public List<Date> findUniqueDates() {
//        EntityManager em;
//        em = this.getEntityManager();
//        List<News> newsList = (List<News>)em.createNamedQuery("News.findUniqueDates").getResultList();
//        return newsList; 
        EntityManager em;
        em = this.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Date> criteria = builder.createQuery(Date.class);
        Root<News> newsRoot = criteria.from(News.class);
        criteria.select(newsRoot.get(News_.date)).distinct(true);
        List<Date> newsList = em.createQuery(criteria).getResultList();
        return newsList;

    }

    public List<String> findUniqueEvents() {
        EntityManager em;
        em = this.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<String> criteria = builder.createQuery(String.class);
        Root<News> newsRoot = criteria.from(News.class);
        criteria.select(newsRoot.get(News_.events)).distinct(true);
        List<String> newsList = em.createQuery(criteria).getResultList();
        return newsList;
    }

    public List<News> findNewsByDate(Date date) {
        EntityManager em;
        em = this.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<News> criteria = builder.createQuery(News.class);
        Root<News> newsRoot = criteria.from(News.class);
        criteria.select(newsRoot);
        criteria.where(builder.equal(newsRoot.get("date"), date));
        List<News> newsList = em.createQuery(criteria).getResultList();
//        List<News> newsList = (List<News>)em.createQuery("SELECT n FROM News n WHERE n.date = :newsDate AND n.events = :newsEvent").setParameter("newsEvent", news).setParameter("newsDate", date).getResultList();
        return newsList;
    }

    public List<News> findNewsByWithinPeriod(Date date, Date endDate) {
        EntityManager em;
        em = this.getEntityManager();
//        CriteriaBuilder builder = em.getCriteriaBuilder();
        List<News> newsList = (List<News>) em.createQuery("SELECT c FROM News c WHERE c.date BETWEEN :dateVar AND :endDateVar").setParameter("endDateVar", endDate).setParameter("dateVar", date).getResultList();
        return newsList;
    }

    public List<News> findSameNews(String currency, String events, String importance, Date date) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (List<News>) em.createNamedQuery("News.findSameNews").setParameter("currency", currency).setParameter("events", events).setParameter("importance", importance)
                    .setParameter("date", date).getResultList();

        } finally {
            em.close();
        }
    }

    public boolean findNearestNewsForSpecificInstrument(String instrumentName) {
        EntityManager em = getEntityManager();
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MINUTE, -60);
            Date startDate = calendar.getTime();
            calendar.add(Calendar.MINUTE, +60);
            Date endDate = calendar.getTime();
            Query query = em.createNamedQuery("News.findLatestAndSpecificNews", News.class);
            query.setParameter("currency", instrumentName).setParameter("startDate", startDate).setParameter("endDate", endDate);
            List list = query.getResultList();
            boolean haveNews = false;
            if (!list.isEmpty()) {
                haveNews = true;
            } else {
                haveNews = false;
            }
            return haveNews;
        } finally {
            em.close();
        }
    }

    public int getNewsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<News> rt = cq.from(News.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<News> findNewsByCurrency(String currency) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            List<News> newsList = (List<News>) em.createNamedQuery("News.findByCurrency").setParameter("currency", currency).getResultList();
            return newsList;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }
}
