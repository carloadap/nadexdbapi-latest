/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.exceptions.NonexistentEntityException;
import com.granberg.model.Trend;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class TrendJpaController implements Serializable {

    public TrendJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Trend trend) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(trend);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Trend trend) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            trend = em.merge(trend);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = trend.getTrendId();
                if (findTrend(id) == null) {
                    throw new NonexistentEntityException("The trend with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Trend trend;
            try {
                trend = em.getReference(Trend.class, id);
                trend.getTrendId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The trend with id " + id + " no longer exists.", enfe);
            }
            em.remove(trend);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Trend> findTrendEntities() {
        return findTrendEntities(true, -1, -1);
    }

    public List<Trend> findTrendEntities(int maxResults, int firstResult) {
        return findTrendEntities(false, maxResults, firstResult);
    }

    private List<Trend> findTrendEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Trend.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Trend findTrend(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Trend.class, id);
        } finally {
            em.close();
        }
    }

    public int getTrendCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Trend> rt = cq.from(Trend.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
