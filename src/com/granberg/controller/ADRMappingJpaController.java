/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.ADRMapping;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class ADRMappingJpaController implements Serializable {

    public ADRMappingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ADRMapping ADRMapping) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ADRMapping);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public ADRMapping findInstrumentHistoryByInstrumentName(String instrumentName) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("ADRMapping.findByInstrumentName", ADRMapping.class);
            query.setParameter("instrumentName", instrumentName);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (ADRMapping) query.getSingleResult();
            } else {
                return null;

            }
        } finally {
            em.close();
        }
    }
    
    public void edit(ADRMapping ADRMapping) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ADRMapping = em.merge(ADRMapping);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ADRMapping.getAdrMappingId();
                if (findADRMapping(id) == null) {
                    throw new NonexistentEntityException("The aDRMapping with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ADRMapping ADRMapping;
            try {
                ADRMapping = em.getReference(ADRMapping.class, id);
                ADRMapping.getAdrMappingId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ADRMapping with id " + id + " no longer exists.", enfe);
            }
            em.remove(ADRMapping);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ADRMapping> findADRMappingEntities() {
        return findADRMappingEntities(true, -1, -1);
    }

    public List<ADRMapping> findADRMappingEntities(int maxResults, int firstResult) {
        return findADRMappingEntities(false, maxResults, firstResult);
    }

    private List<ADRMapping> findADRMappingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ADRMapping.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ADRMapping findADRMapping(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ADRMapping.class, id);
        } finally {
            em.close();
        }
    }

    public int getADRMappingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ADRMapping> rt = cq.from(ADRMapping.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
