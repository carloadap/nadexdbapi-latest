/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.SessionAdr;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class SessionAdrJpaController implements Serializable {

    public SessionAdrJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SessionAdr sessionAdr) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(sessionAdr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SessionAdr> findInstrumentHistoryListByInstrumentName(String instrumentName, int sessionId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("SessionAdr.findByInstrumentNameAndSessionId", SessionAdr.class);
            query.setParameter("instrumentName", instrumentName).setParameter("sessionId", sessionId);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<SessionAdr>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public void edit(SessionAdr sessionAdr) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            sessionAdr = em.merge(sessionAdr);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sessionAdr.getSessionAdrId();
                if (findSessionAdr(id) == null) {
                    throw new NonexistentEntityException("The sessionAdr with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SessionAdr sessionAdr;
            try {
                sessionAdr = em.getReference(SessionAdr.class, id);
                sessionAdr.getSessionAdrId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sessionAdr with id " + id + " no longer exists.", enfe);
            }
            em.remove(sessionAdr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SessionAdr> findSessionAdrEntities() {
        return findSessionAdrEntities(true, -1, -1);
    }

    public List<SessionAdr> findSessionAdrEntities(int maxResults, int firstResult) {
        return findSessionAdrEntities(false, maxResults, firstResult);
    }

    private List<SessionAdr> findSessionAdrEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SessionAdr.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SessionAdr findSessionAdr(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SessionAdr.class, id);
        } finally {
            em.close();
        }
    }

    public int getSessionAdrCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SessionAdr> rt = cq.from(SessionAdr.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
