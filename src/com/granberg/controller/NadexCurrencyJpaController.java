/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.controller.exceptions.PreexistingEntityException;
import com.granberg.model.NadexCurrency;
import com.granberg.model.NadexCurrency_;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class NadexCurrencyJpaController implements Serializable {

    public NadexCurrencyJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NadexCurrency nadexCurrency) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nadexCurrency);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNadexCurrency(nadexCurrency.getNadexId()) != null) {
                throw new PreexistingEntityException("NadexCurrency " + nadexCurrency + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NadexCurrency nadexCurrency) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            nadexCurrency = em.merge(nadexCurrency);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = nadexCurrency.getNadexId();
                if (findNadexCurrency(id) == null) {
                    throw new NonexistentEntityException("The nadexCurrency with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NadexCurrency nadexCurrency;
            try {
                nadexCurrency = em.getReference(NadexCurrency.class, id);
                nadexCurrency.getNadexId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nadexCurrency with id " + id + " no longer exists.", enfe);
            }
            em.remove(nadexCurrency);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NadexCurrency> findNadexCurrencyEntities() {
        return findNadexCurrencyEntities(true, -1, -1);
    }

    public List<NadexCurrency> findNadexCurrencyEntities(int maxResults, int firstResult) {
        return findNadexCurrencyEntities(false, maxResults, firstResult);
    }

    private List<NadexCurrency> findNadexCurrencyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NadexCurrency.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NadexCurrency findNadexCurrency(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NadexCurrency.class, id);
        } finally {
            em.close();
        }
    }

    public List<NadexCurrency> findNadexCurrencyContainsName(String name) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (List<NadexCurrency>) em.createNamedQuery("NadexCurrency.findByContainsName").setParameter("name",
                    "%" + name + "%").getResultList();

        } finally {
            em.close();
        }
    }

    public int getNadexCurrencyCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NadexCurrency> rt = cq.from(NadexCurrency.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
