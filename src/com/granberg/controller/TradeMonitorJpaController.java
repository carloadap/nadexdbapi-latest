/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.TradeMonitor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class TradeMonitorJpaController implements Serializable {

    public TradeMonitorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TradeMonitor tradeMonitor) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tradeMonitor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void createArray(TradeMonitor[] tradeMonitorList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (TradeMonitor tradeMonitor : tradeMonitorList) {
                em.persist(tradeMonitor);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TradeMonitor tradeMonitor) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tradeMonitor = em.merge(tradeMonitor);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tradeMonitor.getTradeMonitorId();
                if (findTradeMonitor(id) == null) {
                    throw new NonexistentEntityException("The tradeMonitor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public boolean findIfTradeMonitorExist(Date date) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("TradeMonitor.findByEntryDate", TradeMonitor.class);
            query.setParameter("entryDate", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } finally {
            em.close();
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TradeMonitor tradeMonitor;
            try {
                tradeMonitor = em.getReference(TradeMonitor.class, id);
                tradeMonitor.getTradeMonitorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tradeMonitor with id " + id + " no longer exists.", enfe);
            }
            em.remove(tradeMonitor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TradeMonitor> findTradeMonitorEntities() {
        return findTradeMonitorEntities(true, -1, -1);
    }

    public List<TradeMonitor> findTradeMonitorEntities(int maxResults, int firstResult) {
        return findTradeMonitorEntities(false, maxResults, firstResult);
    }

    private List<TradeMonitor> findTradeMonitorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TradeMonitor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TradeMonitor findTradeMonitor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TradeMonitor.class, id);
        } finally {
            em.close();
        }
    }

    public int getTradeMonitorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TradeMonitor> rt = cq.from(TradeMonitor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
