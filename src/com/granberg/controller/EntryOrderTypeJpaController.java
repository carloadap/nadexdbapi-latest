/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.EntryOrderType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class EntryOrderTypeJpaController implements Serializable {

    public EntryOrderTypeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(EntryOrderType entryOrderType) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entryOrderType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EntryOrderType entryOrderType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            entryOrderType = em.merge(entryOrderType);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = entryOrderType.getEntryOrderTypeId();
                if (findEntryOrderType(id) == null) {
                    throw new NonexistentEntityException("The entryOrderType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EntryOrderType entryOrderType;
            try {
                entryOrderType = em.getReference(EntryOrderType.class, id);
                entryOrderType.getEntryOrderTypeId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The entryOrderType with id " + id + " no longer exists.", enfe);
            }
            em.remove(entryOrderType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EntryOrderType> findEntryOrderTypeEntities() {
        return findEntryOrderTypeEntities(true, -1, -1);
    }

    public List<EntryOrderType> findEntryOrderTypeEntities(int maxResults, int firstResult) {
        return findEntryOrderTypeEntities(false, maxResults, firstResult);
    }

    private List<EntryOrderType> findEntryOrderTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EntryOrderType.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EntryOrderType findEntryOrderType(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EntryOrderType.class, id);
        } finally {
            em.close();
        }
    }

    public int getEntryOrderTypeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EntryOrderType> rt = cq.from(EntryOrderType.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
