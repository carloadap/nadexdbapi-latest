/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Contract;
import com.granberg.model.InstrumentType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class ContractJpaController implements Serializable {

    public ContractJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Contract contract) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(contract);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Contract> findContractListBySymbolAndExpirationDate(Contract[] contract) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            List<Contract> contractList = new LinkedList<>();
            for (Contract record : contractList) {
                Query query = em.createNamedQuery("Contract.findContractListBySymbolAndExpirationDate", Contract.class);
                query.setParameter("contractSymbol", record.getContractSymbol()).setParameter("expirationDate", record.getExpirationDate());
                List list = query.getResultList();
                if (!list.isEmpty()) {
                    List<Contract> newContractList = (List<Contract>) list;
                    for (Contract newContract : newContractList) {
                        contractList.add(newContract);
                    }
                }
            }
            return contractList;
//            return (InstrumentMapping) em.createNamedQuery("InstrumentMapping.findByAlternateName").setParameter("alternateName",
//          name).getSingleResult();

        } finally {
            em.close();
        }
    }

    public void createContractCurrencyOnly(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId, int periodicityId) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (Contract record : contract) {
                if (record.getInstrumentId() != null) {
                    if (record.getInstrumentId() == instrumentId && record.getPeriodicityId() == periodicityId) {
                        em.persist(record);
                    }
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void createContractBullSpreadOnly(Contract[] contractList, int contractTypeId) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            for (Contract record : contractList) {
                em.getTransaction().begin();
                Query query = em.createNamedQuery("Contract.findByContractSymbolExpirationDateTest", Contract.class);
                query.setParameter("contractSymbol", record.getContractSymbol()).setParameter("expirationDate", record.getExpirationDate());
                List list = query.getResultList();
                if (list.isEmpty()) {
                    if (record.getInstrumentId() != null) {
                        if (record.getContractTypeId() == contractTypeId && record.getExpirationDate() != null && record.getOpeningDate() != null) {
                            em.persist(record);
                            em.getTransaction().commit();
                        }
                    }
                } else {
                    Contract contract = (Contract) query.getSingleResult();
                    contract.setSecurityId(contract.getSecurityId());
                    em.getTransaction().commit();
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void createContractCurrencyOnly(Contract[] contract, String namedQuery, int instrumentId, int instrumentTypeId) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (Contract record : contract) {
                if (record.getInstrumentId() != null) {
                    Query query = em.createNamedQuery(namedQuery, InstrumentType.class);
                    query.setParameter("instrumentId", record.getInstrumentId()).setParameter("instrumentTypeId", instrumentTypeId);
                    List list = query.getResultList();
                    if (record.getInstrumentId() == instrumentId) {
                        em.persist(record);
                    }
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateSettledContract(String contractSymbol) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Contract.findByContractSymbol", Contract.class);
            query.setParameter("contractSymbol", contractSymbol);
            List<Contract> contractList = (List<Contract>) query.getResultList();
            for (Contract contract : contractList) {
                contract.setExpirationDate(null);
                em.getTransaction().commit();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Contract contract) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            contract = em.merge(contract);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = contract.getContractId();
                if (findContract(id) == null) {
                    throw new NonexistentEntityException("The contract with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contract contract;
            try {
                contract = em.getReference(Contract.class, id);
                contract.getContractId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contract with id " + id + " no longer exists.", enfe);
            }
            em.remove(contract);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void deleteContract(Contract contract) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contract refContract;
            Query query = em.createNamedQuery("Contract.findByContractSymbolExpirationDateTest", Contract.class);
            query.setParameter("contractSymbol", contract.getContractSymbol()).setParameter("expirationDate", contract.getExpirationDate());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                refContract = (Contract) query.getSingleResult();
                em.remove(refContract);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateCurrentContract(Contract contract, int contractTypeId) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Contract.findByContractSymbolExpirationDateTest", Contract.class);
            query.setParameter("contractSymbol", contract.getContractSymbol()).setParameter("expirationDate", contract.getExpirationDate());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                Contract refContract = (Contract) query.getSingleResult();
                refContract.setSecurityId(contract.getSecurityId());
                em.getTransaction().commit();
            } else {
                if (contract.getContractTypeId() == contractTypeId && contract.getExpirationDate() != null && contract.getOpeningDate() != null) {
                    em.persist(contract);
                    em.getTransaction().commit();
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Contract> findContractEntities() {
        return findContractEntities(true, -1, -1);
    }

    public List<Contract> findContractEntities(int maxResults, int firstResult) {
        return findContractEntities(false, maxResults, firstResult);
    }

    private List<Contract> findContractEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Contract.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Contract findContract(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Contract.class, id);
        } finally {
            em.close();
        }
    }

    public Contract findNotExpiredContract(int contractId, Date date) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Contract.findByContractIdNotExpired", Contract.class);
            query.setParameter("contractId", contractId).setParameter("expirationDate", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Contract) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Contract findNotExpiredContractBySymbol(String contractSymbol, Date date) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Contract.findByContractSymbolNotExpired", Contract.class);
            query.setParameter("contractSymbol", contractSymbol).setParameter("expirationDate", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Contract) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<Contract> findContractByStrikePrice(BigDecimal strikePrice) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findByStrikePrice", Contract.class);
            query.setParameter("strikePrice", strikePrice);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Contract>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<Contract> findContractByCotractTypeAndInstrumentId(int contractTypeId, int instrumentId) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findActiveContractByContractTypeIdAndInstrumentId", Contract.class);
            query.setParameter("contractTypeId", contractTypeId).setParameter("instrumentId", instrumentId).setParameter("currentDate", new Date());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Contract>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<Contract> findContractNotExpired() {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findNotExpired", Contract.class);
            query.setParameter("expirationDate", new Date());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Contract>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Contract findContractByStrikePriceAndPeriod(BigDecimal strikePrice, int period) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findByStrikePriceAndPeriod", Contract.class);
            query.setParameter("strikePrice", strikePrice).setParameter("periodicityId", period);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Contract) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<Contract> findContractByCotractTypeAndInstrumentIdTest(int contractTypeId, int instrumentId) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findActiveContractByContractTypeIdAndInstrumentIdTest", Contract.class);
            query.setParameter("contractTypeId", contractTypeId).setParameter("instrumentId", instrumentId);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Contract>) query.getResultList();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Contract findContractByStrikePriceAndExpirationDate(BigDecimal strikePrice, Date expirationDate) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Contract contract = (Contract) em.createNamedQuery("Contract.findByStrikePriceAndExpirationDate").setParameter("strikePrice",
                    strikePrice).setParameter("expirationDate", expirationDate).getSingleResult();
            if (contract != null) {
                return contract;
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public Contract findContractTypeBySymbol(String name) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("Contract.findByContractSymbol", Contract.class);
            query.setParameter("contractSymbol", name);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Contract) query.getSingleResult();
            } else {
                return null;
            }
        } finally {
            em.close();
        }
    }

    public List<Contract> findContractListBySymbol(String name) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (List<Contract>) em.createNamedQuery("Contract.findByContractSymbol").setParameter("contractSymbol",
                    name).getResultList();

        } finally {
            em.close();
        }
    }

    public List<Contract> findContractListByInstrumentId(int instrumendId) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (List<Contract>) em.createNamedQuery("Contract.findByContractInstrumentId").setParameter("instrumentId",
                    instrumendId).setParameter("expirationDate", new Date()).getResultList();

        } finally {
            em.close();
        }
    }

    public int getContractCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Contract> rt = cq.from(Contract.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
