/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.InstrumentMapping;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class InstrumentMappingJpaController implements Serializable {

    public InstrumentMappingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InstrumentMapping instrumentMapping) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instrumentMapping);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InstrumentMapping instrumentMapping) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            instrumentMapping = em.merge(instrumentMapping);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = instrumentMapping.getInstrumentMappingId();
                if (findInstrumentMapping(id) == null) {
                    throw new NonexistentEntityException("The instrumentMapping with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InstrumentMapping instrumentMapping;
            try {
                instrumentMapping = em.getReference(InstrumentMapping.class, id);
                instrumentMapping.getInstrumentMappingId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrumentMapping with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrumentMapping);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InstrumentMapping> findInstrumentMappingEntities() {
        return findInstrumentMappingEntities(true, -1, -1);
    }

    public List<InstrumentMapping> findInstrumentMappingEntities(int maxResults, int firstResult) {
        return findInstrumentMappingEntities(false, maxResults, firstResult);
    }

    private List<InstrumentMapping> findInstrumentMappingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InstrumentMapping.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InstrumentMapping findInstrumentMapping(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InstrumentMapping.class, id);
        } finally {
            em.close();
        }
    }
    
    public InstrumentMapping findInstrumentByName(String name){
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("InstrumentMapping.findByAlternateName", InstrumentMapping.class);
            query.setParameter("alternateName", name);
            List list = query.getResultList();
            if(!list.isEmpty()){
               return (InstrumentMapping) query.getSingleResult();
            }else{
                return null;
            }
//            return (InstrumentMapping) em.createNamedQuery("InstrumentMapping.findByAlternateName").setParameter("alternateName",
//          name).getSingleResult();
            
        } finally {
            em.close();
        }
    }

    public int getInstrumentMappingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InstrumentMapping> rt = cq.from(InstrumentMapping.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
