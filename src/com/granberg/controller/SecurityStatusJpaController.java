/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.SecurityStatus;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class SecurityStatusJpaController implements Serializable {

    public SecurityStatusJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SecurityStatus securityStatus) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(securityStatus);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SecurityStatus securityStatus) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            securityStatus = em.merge(securityStatus);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = securityStatus.getSecurityStatusId();
                if (findSecurityStatus(id) == null) {
                    throw new NonexistentEntityException("The securityStatus with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SecurityStatus securityStatus;
            try {
                securityStatus = em.getReference(SecurityStatus.class, id);
                securityStatus.getSecurityStatusId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The securityStatus with id " + id + " no longer exists.", enfe);
            }
            em.remove(securityStatus);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SecurityStatus> findSecurityStatusEntities() {
        return findSecurityStatusEntities(true, -1, -1);
    }

    public List<SecurityStatus> findSecurityStatusEntities(int maxResults, int firstResult) {
        return findSecurityStatusEntities(false, maxResults, firstResult);
    }

    private List<SecurityStatus> findSecurityStatusEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SecurityStatus.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SecurityStatus findSecurityStatus(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SecurityStatus.class, id);
        } finally {
            em.close();
        }
    }

    public int getSecurityStatusCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SecurityStatus> rt = cq.from(SecurityStatus.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
