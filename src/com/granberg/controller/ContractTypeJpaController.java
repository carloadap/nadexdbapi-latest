/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.ContractType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class ContractTypeJpaController implements Serializable {

    public ContractTypeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ContractType contractType) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(contractType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ContractType contractType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            contractType = em.merge(contractType);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = contractType.getContractTypeId();
                if (findContractType(id) == null) {
                    throw new NonexistentEntityException("The contractType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ContractType contractType;
            try {
                contractType = em.getReference(ContractType.class, id);
                contractType.getContractTypeId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contractType with id " + id + " no longer exists.", enfe);
            }
            em.remove(contractType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ContractType> findContractTypeEntities() {
        return findContractTypeEntities(true, -1, -1);
    }

    public List<ContractType> findContractTypeEntities(int maxResults, int firstResult) {
        return findContractTypeEntities(false, maxResults, firstResult);
    }

    private List<ContractType> findContractTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ContractType.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ContractType findContractType(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ContractType.class, id);
        } finally {
            em.close();
        }
    }
    
    public ContractType findContractTypeByName(String name){
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (ContractType) em.createNamedQuery("ContractType.findByNadexCode").setParameter("nadexCode",
          name).getSingleResult();
            
        } finally {
            em.close();
        }
    }

    public int getContractTypeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ContractType> rt = cq.from(ContractType.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
