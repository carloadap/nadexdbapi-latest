/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Adr;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class AdrJpaController implements Serializable {

    public AdrJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Adr adr) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(adr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Adr adr) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            adr = em.merge(adr);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = adr.getAdrId();
                if (findAdr(id) == null) {
                    throw new NonexistentEntityException("The adr with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Date findSessionLatestADR(int sessionId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Adr.findBySessionLatestADR", Adr.class);
            query.setParameter("sessionId", sessionId);
            Date date = (Date) query.getSingleResult();
            return date;
        } finally {
            em.close();
        }
    }

    public Date findLatestADR() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Adr.findByLatestADR", Adr.class);
            Date date = (Date) query.getSingleResult();
            return date;
        } finally {
            em.close();
        }
    }

    public void createArray(Adr[] adrList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (Adr adr : adrList) {
                em.persist(adr);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateAdrHighLow(Adr adr) {
        EntityManager em = getEntityManager();
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Adr.findByInstrumentNameAndDate", Adr.class);
            query.setParameter("instrumentName", adr.getInstrumentName()).setParameter("date", adr.getDate());
            Adr adr1 = (Adr) query.getSingleResult();
            adr1.setCurrentHigh(adr.getCurrentHigh());
            adr1.setCurrentLow(adr.getCurrentLow());
            adr1.setExhaustion(adr.getExhaustion());
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void updateAdr(Adr adr) {
        EntityManager em = getEntityManager();
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("Adr.findByInstrumentName", Adr.class);
            query.setParameter("instrumentName", adr.getInstrumentName());
            Adr adr1 = (Adr) query.getSingleResult();
            adr1.setAdr(adr.getAdr());
            adr1.setHighLimit(adr.getHighLimit());
            adr1.setLowLimit(adr.getLowLimit());
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public Adr findInstrumentHistoryByInstrumentName(String instrumentName) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Adr.findByInstrumentName", Adr.class);
            query.setParameter("instrumentName", instrumentName);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Adr) query.getSingleResult();
            } else {
                return null;

            }
        } finally {
            em.close();
        }
    }

    public List<Adr> findLatestRunningADR(Date date, int sessionId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Adr.findByDateAndSessionId", Adr.class);
            query.setParameter("date", date).setParameter("sessionId", sessionId);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<Adr>) list;
            } else {
                return null;

            }
        } finally {
            em.close();
        }
    }

    public Adr findLatestRunningADR(String instrumentName, Date date, int sessionId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Adr.findByInstrumentNameAndDateAndSessionId", Adr.class);
            query.setParameter("date", date).setParameter("sessionId", sessionId).setParameter("instrumentName", instrumentName);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (Adr) query.getSingleResult();
            } else {
                return null;

            }
        } finally {
            em.close();
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Adr adr;
            try {
                adr = em.getReference(Adr.class, id);
                adr.getAdrId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The adr with id " + id + " no longer exists.", enfe);
            }
            em.remove(adr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Adr> findAdrEntities() {
        return findAdrEntities(true, -1, -1);
    }

    public List<Adr> findAdrEntities(int maxResults, int firstResult) {
        return findAdrEntities(false, maxResults, firstResult);
    }

    private List<Adr> findAdrEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Adr.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Adr findAdr(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Adr.class, id);
        } finally {
            em.close();
        }
    }

    public int getAdrCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Adr> rt = cq.from(Adr.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
