/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Contract;
import com.granberg.model.MarketData;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class MarketDataJpaController implements Serializable {

    public MarketDataJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MarketData marketData) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(marketData);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void updateMarketData(MarketData marketData, int columnToUpdate) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery("MarketData.findBySymbolSortByContractId", MarketData.class);
            query.setParameter("symbol", marketData.getSymbol());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                List<MarketData> marketDataList = (List<MarketData>) query.getResultList();
                MarketData marketDataToUpdate = marketDataList.get(0);
                if (marketData.getBid() != null && marketData.getOffer() != null) {
                    marketDataToUpdate.setBid(marketData.getBid());
                    marketDataToUpdate.setOffer(marketData.getOffer());
                } else {
                    switch (columnToUpdate) {
                        case 0:
                            marketDataToUpdate.setBid(marketData.getBid());
                            marketDataToUpdate.setSendingTime(marketData.getSendingTime());
                            break;
                        case 1:
                            marketDataToUpdate.setOffer(marketData.getOffer());
                            marketDataToUpdate.setSendingTime(marketData.getSendingTime());
                            break;
                    }
                }
                em.getTransaction().commit();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void createMarketData(MarketData marketData, String namedQuery) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Query query = em.createNamedQuery(namedQuery, Contract.class);
            query.setParameter("contractId", marketData.getContractId());
            List list = query.getResultList();
            if (!list.isEmpty()) {
                Contract contract = (Contract) query.getSingleResult();
                marketData.setContractId(contract.getContractId());
                em.persist(marketData);
                em.getTransaction().commit();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MarketData marketData) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            marketData = em.merge(marketData);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = marketData.getMarketDataId();
                if (findMarketData(id) == null) {
                    throw new NonexistentEntityException("The marketData with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MarketData marketData;
            try {
                marketData = em.getReference(MarketData.class, id);
                marketData.getMarketDataId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The marketData with id " + id + " no longer exists.", enfe);
            }
            em.remove(marketData);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MarketData> findMarketDataEntities() {
        return findMarketDataEntities(true, -1, -1);
    }

    public List<MarketData> findMarketDataEntities(int maxResults, int firstResult) {
        return findMarketDataEntities(false, maxResults, firstResult);
    }

    private List<MarketData> findMarketDataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MarketData.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MarketData findMarketData(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MarketData.class, id);
        } finally {
            em.close();
        }
    }

    public MarketData findMarketDataByContractId(int contradId) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            return (MarketData) em.createNamedQuery("MarketData.findByContractId").setParameter("contractId",
                    contradId).getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    public MarketData findMarketDataByContractSymBol(String contractSymbol) {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("MarketData.findBySymbol", MarketData.class);
            query.setParameter("symbol", contractSymbol);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (MarketData) query.getSingleResult();
            } else {
                return null;
            }
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    public List<MarketData> findMarketDataBidOffetNotNull() {
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Query query = em.createNamedQuery("MarketData.findAllBidOfferEntryNotNull", MarketData.class);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return (List<MarketData>) query.getResultList();
            } else {
                return null;
            }
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }

    public int getMarketDataCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MarketData> rt = cq.from(MarketData.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
