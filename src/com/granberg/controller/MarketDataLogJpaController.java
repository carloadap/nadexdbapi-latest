/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.MarketDataLog;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class MarketDataLogJpaController implements Serializable {

    public MarketDataLogJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MarketDataLog marketDataLog) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(marketDataLog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MarketDataLog marketDataLog) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            marketDataLog = em.merge(marketDataLog);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = marketDataLog.getMarketDataLogId();
                if (findMarketDataLog(id) == null) {
                    throw new NonexistentEntityException("The marketDataLog with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MarketDataLog marketDataLog;
            try {
                marketDataLog = em.getReference(MarketDataLog.class, id);
                marketDataLog.getMarketDataLogId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The marketDataLog with id " + id + " no longer exists.", enfe);
            }
            em.remove(marketDataLog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MarketDataLog> findMarketDataLogEntities() {
        return findMarketDataLogEntities(true, -1, -1);
    }

    public List<MarketDataLog> findMarketDataLogEntities(int maxResults, int firstResult) {
        return findMarketDataLogEntities(false, maxResults, firstResult);
    }

    private List<MarketDataLog> findMarketDataLogEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MarketDataLog.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Date findMarketDataLogBySymbol(String symbol, int priceUpdated){
        EntityManager em = getEntityManager();
        //em.createNamedQuery("Instrument.findByInstrumentName");
        try {
            Date date = (Date)em.createNamedQuery("MarketDataLog.findByLatestSymbol").setParameter("symbol",
          symbol).setParameter("priceUpdated", priceUpdated).getSingleResult();
            if(date !=null){
                return date;
            }else{
                return null;
            }
            
        } finally {
            em.close();
        }
    }

    public MarketDataLog findMarketDataLog(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MarketDataLog.class, id);
        } finally {
            em.close();
        }
    }

    public int getMarketDataLogCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MarketDataLog> rt = cq.from(MarketDataLog.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
