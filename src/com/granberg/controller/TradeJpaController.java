/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.controller.exceptions.PreexistingEntityException;
import com.granberg.model.Trade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class TradeJpaController implements Serializable {

    public TradeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Trade trade) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(trade);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTrade(trade.getTradeId()) != null) {
                throw new PreexistingEntityException("Trade " + trade + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void createArray(Trade[] tradeList) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            for (Trade trade : tradeList) {
                em.persist(trade);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public boolean findIfTradeExist(Date date, int sessionId) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Trade.findBySessionIdAndDate", Trade.class);
            query.setParameter("sessionId", sessionId).setParameter("date", date);
            List list = query.getResultList();
            if (!list.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } finally {
            em.close();
        }
    }

    public void edit(Trade trade) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            trade = em.merge(trade);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = trade.getTradeId();
                if (findTrade(id) == null) {
                    throw new NonexistentEntityException("The trade with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Trade trade;
            try {
                trade = em.getReference(Trade.class, id);
                trade.getTradeId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The trade with id " + id + " no longer exists.", enfe);
            }
            em.remove(trade);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Trade> findTradeEntities() {
        return findTradeEntities(true, -1, -1);
    }

    public List<Trade> findTradeEntities(int maxResults, int firstResult) {
        return findTradeEntities(false, maxResults, firstResult);
    }

    private List<Trade> findTradeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Trade.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Trade findTrade(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Trade.class, id);
        } finally {
            em.close();
        }
    }

    public int getTradeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Trade> rt = cq.from(Trade.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
